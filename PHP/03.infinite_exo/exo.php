<?php

## Ces conditions retournent-elles « true » ou « false » en PHP ?

// (1=='1') ""true""

// (1==='1') ""false""

// (1!= '1 train passe') "true"

// (1!==2) true

// (($i=1)==1) true

// Si nous avons : $a=1 ;$b=1 ;
// Que retourne :
// ($b == $a++) ; true
// ($b == ++$a) ; false

## Que vont afficher les lignes de codes a,b,c,d si nous avons :

## Que vont afficher les lignes de codes commançant par `echo` si nous avons :

// $texte = '1'; echo ($texte); '1'
// $i='texte';   echo ($i); 'texte'

$nbrs = 99;
$letterP = chr(65);
$letterPmax = chr(80);
$letterPtwo = chr(65);
$letterPmaxtwo = chr(80);
$letterH = chr(65);
$letterHmax = chr(72);
// $letterStart = chr(0);
$Department = 04;
// 65 a 90
chr($letterP);

echo '## Boucle 2 - Imbrication </br>';
while ($nbrs < 120 || $letterP < $letterPmax || $letterH < $letterHmax || $letterPtwo < $letterPmaxtwo) {

    if ($nbrs < 120) {
        $nbrs++;
    }
    if ($letterP < $letterPmax) {
        $letterP++;
    }
    if ($letterH < $letterHmax) {
        $letterH++;
    }
    if ($letterPtwo < $letterPmaxtwo) {
        $letterPtwo++;
    }


    echo $nbrs;
    echo " ";

    echo $letterP;
    echo $letterH;
    echo $letterPtwo;
    echo " ";

    echo $Department;
    echo "</br>";
}

echo "</br>";
echo "</br>";
echo "</br>";






// for ($nbrs = 100; $nbrs < 121; $nbrs++) {


//     for ($letterP = chr(65); $letterP < $letterPmax; $letterP++) {


//         for ($letterH = chr(65); $letterH < $letterHmax; $letterH++) {
//         }
//         for ($letterPtwo = chr(65); $letterPtwo < $letterPmaxtwo; $letterPtwo++) {
//         }
//     }
//     echo $nbrs;
//     echo " ";
//     echo $letterP;
//     echo $letterH;
//     echo $letterPtwo;
//     echo "</br>";
// }

echo '## Condition 2';

$age = $_GET['age'];
$sexe = "femme";

$selected = $_GET['sexe'];

if (intval($_GET['age']) > 21 && intval($_GET['age']) < 40) {

    if ($selected == 2) {
        echo ' </br> Bienvenue, femme agé de ' . $_GET['age'] . ' ans !!';
    }
}
var_dump(($_GET['sexe']));
echo $selected;

echo "</br>";
echo "</br>";
echo "</br>";
echo '## Boucle encore';
echo "</br>";

// $nbrStr = rand(0, 10);
// $count = 1;

// while ($nbrStr < 10) {
//     echo "</br>";
//     echo $count++ . ' ';
//     echo $nbrStr;

//     if ($nbrStr == 10) {
//         break;
//     }
// }

$i = 1;
$random = 0;
while ($random != 10) {
    $random = rand(1, 10);
    $i;

    echo "</br>";
    echo " nombre de jet est egale a  " . $i++ . "</br>";
    echo $random . " est le nombre qui a été random";
}

echo "</br>";
echo "The number is " . $random . " Bravo ! <br>";

echo "</br>";
echo "</br>";
echo "</br>";
echo '## Chaines';
echo "</br>";
