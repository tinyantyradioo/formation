0# EXO PHP ALGO

## Ces conditions retournent-elles « true » ou « false » en PHP ?

    (1=='1') ""true""

    (1==='1') ""false""

    (1!= '1 train passe') "false"

    (1!==2) true

    (($i=1)==1) true

    Si nous avons : $a=1 ;$b=1 ;
    Que retourne :
    ($b == $a++) ; 2
    ($b == ++$a) ; 2

## Que vont afficher les lignes de codes a,b,c,d si nous avons :

## Que vont afficher les lignes de codes commançant par `echo` si nous avons :

    $texte = '1'; echo ($texte);
    $i='texte';   echo ($i);

## Condition 2

    Écrire une expression conditionnelle utilisant les variables $age et $sexe dans une instruction if pour sélectionner une personne de sexe féminin dont l’age est compris entre 21 et 40 ans et afficher un message de bienvenue approprié. Les données proviennent d’un formulaire.
    Écrire une expression conditionnelle utilisant les variables $age et $sexe dans une instruction if pour sélectionner une personne de sexe féminin dont l’age est compris entre 21 et 40 ans et afficher un message de bienvenue approprié.
    Les données proviennent d’un formulaire en mode GET.

## Boucle

## Boucle 2 - Imbrication

    Créer et afficher des numéros d’immatriculation automobile (pour Digne, par exemple) en commençant au numéro 100 PHP 04
    Créer et afficher des numéros d’immatriculation automobile (pour Manosque, par exemple) en commençant au numéro 100 PHP 04

    Si on réalise le script complet, il affiche plusieurs millions de numéros de 100 PHP 04 à 999 ZZZ 04. L’exécution est donc très longue et risque de bloquer le serveur. Pour effectuer un test, les valeurs dwhilimitées au départ entre 100 et 120.

    Pour information en ASCII la lettre "A" a comme valeur 65, "B" comme valeur 66 ...etc.
    L'instruction "echo chr(65);" affiche donc A
    L'instruction "echo chr(65);" affiche donc A !

## Boucle encore
