<?php

/** Configuration */
require('config/config.php');

/** Librairie BDD */
require('lib/db.php');

/** Librairie APP */
require('lib/app.php');


/** Les modèles */
require('models/Article.php');


$view = 'home';
$pageTitle = 'Bienvenue !';

/** On intancie le modèle Article */
$modelArticle = new Article();

/** On récupère les 10 derniers articles valides triés par date de publication DESC */
$articles = $modelArticle->getAll(10,0,['a_published_at'=>'DESC'],true);


require('views/layout.phtml');