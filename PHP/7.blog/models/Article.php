<?php



/**
 * Article
 */
class Article {
    
    private $dbh;

    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->dbh = connect();
    }

    /** Retourne toutes les catégories
     * @return array|false le jeu d'enregistrement ou false si une erreur survient
     */
    public function getAll(?int $limit = null, int $start=0,  array $orders=[], bool $valid=false)
    {
        try {
            /** On prépare notre requête SQL en fonction des paramètre */
            $sql = 'SELECT a.*,c.c_title, u.u_firstname, u.u_lastname FROM article a
            INNER JOIN category c ON (c.c_id = a.category_id)
            INNER JOIN user u ON(u.u_id = a.user_id)';
            $sqlWhere = '';
            $sqlOrderBy = '';
            $sqlLimit = '';
           
            

            // On créé une CLAUSE LIMIT si nécessaire
            if(!empty($limit))
                $sqlLimit = ' LIMIT :limit OFFSET :start';

            // On créé une CLAUSE ORDER BY si nécessaire (le tableau contient le champ à trier comme index et l'ordre de tri comme valeur)
            // ['a_title'=>'DESC','a_published_at'=>'ASC']
            if(!empty($orders)) {
                $sqlOrderBy = ' ORDER BY';
                foreach($orders as $colName=>$order) {
                    $sqlOrderBy.= " $colName $order";
                    // Si on est pas à la dernière itération on ajoute une virgule pour le order suivant !
                    // Attention ; à patir de php 7.3 et >
                    if(array_key_last($orders) != $colName)
                         $sqlOrderBy.=',';
                }
            }

            // On créé une clause WHERE si on doit filtrer les article valide !
            if($valid === true) {
                $sqlWhere = ' WHERE a_status = 1';
            }

            // On forme la requête SQL définitive !
            $sql .= $sqlWhere . $sqlOrderBy . $sqlLimit;

            //var_dump($sql);

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare($sql);

            /** On bind si nécessaire pour le LIMIT */
            if (!empty($limit)) {
                $sth->bindValue('limit',$limit, PDO::PARAM_INT);
                $sth->bindValue('start', $start, PDO::PARAM_INT);
            }

            /** 3 : on exécute la requête SQL ! */
            $sth->execute();

            /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
            return $sth->fetchAll();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }

    }

    /** Retourne toutes les catégories
     * @param int $id identfiant de la catégorie
     * @return array|false le jeu d'enregistrement ou false si une erreur survient
     */
    public function getById(int $id)
    {
        try {

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare('SELECT * FROM article WHERE a_id = :id');

            /** 3 : on exécute la requête SQL ! */
            $sth->bindValue('id',$id, PDO::PARAM_INT);
            $sth->execute();

            /** 4 : on récupère la ligne du jeu d'enregistrement */
            return $sth->fetch();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }
    }

    /** Retourne une catégorie par rapport à son slug
     * @param string $slug le slug recherché dans la table category
     * @param  int|null $excludeId
     * @return array
     */
    public function getBySlug(string $slug, int $excludeId=null) {
        try {

            $sth = $this->dbh->prepare('SELECT a_slug 
            FROM article 
            WHERE a_slug = :slug AND a_id != :id');

            $sth->bindValue('slug', $slug, PDO::PARAM_STR);
            $sth->bindValue('id', $excludeId, PDO::PARAM_INT);

            $sth->execute();

            return $sth->fetch();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }
    
    /**
     * getByCategoryId
     *
     * @param  int $id
     * @return void
     */
    public function getByCategoryId(int $id) {
        try {

            $sth = $this->dbh->prepare('SELECT * 
            FROM article 
            WHERE category_id = :id');

            $sth->bindValue('id', $id, PDO::PARAM_INT);

            $sth->execute();

            return $sth->fetchAll();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    public function getByUserId(int $id)
    {
        try {

            $sth = $this->dbh->prepare('SELECT * 
            FROM article 
            WHERE user_id = :id');

            $sth->bindValue('id', $id, PDO::PARAM_INT);

            $sth->execute();

            return $sth->fetchAll();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }





    /**
     * add
     *
     * @param  string $title
     * @param  DateTime $modifiedAt
     * @param  DateTime $publishedAt
     * @param  string $slug
     * @param  int $category
     * @param  int $user
     * @param  string $content
     * @param  string $picture
     * @return void
     */
    public function add(string $title, DateTime $createdAt, DateTime $publishedAt, string $slug, int $category, int $user, ?string $content =null, ?string $picture = null) {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('INSERT INTO article (a_title, a_content, a_created_at, a_published_at, a_picture, a_slug, category_id, user_id) 
                VALUES (:title,:content, :createdAt,:publishedAt, :picture, :slug, :category, :user)');


            //3. Lier les données
            $sth->bindValue('title', $title, PDO::PARAM_STR);
            $sth->bindValue('content', $content, PDO::PARAM_STR);
            $sth->bindValue('createdAt', $createdAt->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('publishedAt', $publishedAt->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('picture', $picture, PDO::PARAM_STR);
            $sth->bindValue('slug', $slug, PDO::PARAM_STR);
            $sth->bindValue('category', $category, PDO::PARAM_INT);
            $sth->bindValue('user', $user, PDO::PARAM_INT);

            //4. Executer ma requête
            $sth->execute();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }


    /**
     * update
     *
     * @param  int $id
     * @param  string $title
     * @param  DateTime $modifiedAt
     * @param  DateTime $publishedAt
     * @param  string $slug
     * @param  int $category
     * @param  int $user
     * @param  string $content
     * @param  string $picture
     * @return void
     */
    public function update(int $id, string $title, DateTime $modifiedAt, DateTime $publishedAt, string $slug, int $category, int $user, ?string $content =null, ?string $picture = null)
    {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('UPDATE article 
            SET a_title=:title, a_content=:content, a_modified_at=:modifiedAt, a_published_at=:publishedAt, a_picture=:picture, a_slug=:slug, category_id=:category, user_id=:user
            WHERE a_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, PDO::PARAM_INT);
            $sth->bindValue('title', $title, PDO::PARAM_STR);
            $sth->bindValue('content', $content, PDO::PARAM_STR);
            $sth->bindValue('modifiedAt', $modifiedAt->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('publishedAt', $publishedAt->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('picture', $picture, PDO::PARAM_STR);
            $sth->bindValue('slug', $slug, PDO::PARAM_STR);
            $sth->bindValue('category', $category, PDO::PARAM_INT);
            $sth->bindValue('user', $user, PDO::PARAM_INT);

            //4. Executer ma requête
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    /** Supprime un article
     * @param int $id identifiant de l'article
     */
    public function delete(int $id) {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('DELETE FROM article 
            WHERE a_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, PDO::PARAM_INT);

            //4. Executer ma requête
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }
}