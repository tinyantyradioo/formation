<?php



/**
 * User
 */
class User {
    
    private $dbh;

    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->dbh = connect();
    }

    public function getAll() {
        try {

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare('SELECT * FROM user');

            /** 3 : on exécute la requête SQL ! */
            $sth->execute();

            /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
            return $sth->fetchAll();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }

    }


    /** Retourne un utilisateur correspondant à l'id passé en param
     * @param int $id l'id recherché dans la table user
     */
    public function getById(int $id)
    {
        try {

            $sth = $this->dbh->prepare('SELECT * FROM user WHERE u_id = :id');

            $sth->bindValue('id', $id, PDO::PARAM_INT);

            $sth->execute();

            $values = $sth->fetch();

            return $values;

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    /** Retourne un utilisateur correspondant à l'email passé en param
     * @param string $email l'email recherché dans la table user
     * @param  int|null $excludeId
     */
    public function getByEmail(string $email, int $excludeId=null) {
        try {

            $sth = $this->dbh->prepare('SELECT * 
            FROM user 
            WHERE u_email = :email AND u_id != :id');

            $sth->bindValue('email', $email, PDO::PARAM_STR);
            $sth->bindValue('id', $excludeId, PDO::PARAM_INT);

            $sth->execute();

            $values = $sth->fetch();

            return $values;

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }


    /** Ajoute un utilisateur dans la base de donnée
     * @param string $lastname
     * @param string $firstname
     * @param string $email
     * @param string $password
     * @param DateTime $createdAt
     * @param bool $status
     * @param string|null $avatar
     * @param string $bio
     * @param string $role
     * 
     * @return mixed
     */
    public function add(string $lastname, string $firstname, string $email, string $password, DateTime $createdAt, bool $status = true, ?string $avatar = null, string $bio = '', string $role = 'ROLE_AUTH') {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('INSERT INTO user (u_firstname, u_lastname, u_email, u_password, u_created_at, u_role, u_status, u_avatar, u_bio) 
                VALUES (:firstname,:lastname, :email, :password, :createdAt, :role, :status, :avatar, :bio )');


            //3. Lier les données
            $sth->bindValue('firstname', $firstname, PDO::PARAM_STR);
            $sth->bindValue('lastname', $lastname, PDO::PARAM_STR);
            $sth->bindValue('email', $email, PDO::PARAM_STR);
            $sth->bindValue('password', $password, PDO::PARAM_STR);
            $sth->bindValue('createdAt', $createdAt->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('role', $role, PDO::PARAM_STR);
            $sth->bindValue('status', $status, PDO::PARAM_INT);
            $sth->bindValue('avatar', $avatar, PDO::PARAM_STR);
            $sth->bindValue('bio', $bio, PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    public function update(int $id, string $lastname, string $firstname, string $email, string $password, bool $status = true, ?string $avatar = null, string $bio = '', string $role = 'ROLE_AUTH')
    {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('UPDATE user 
            SET u_firstname=:firstname, u_lastname=:lastname, u_email=:email, u_password=:password, u_role=:role, u_status=:status, u_avatar=:avatar, u_bio=:bio
            WHERE u_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, PDO::PARAM_INT);
            $sth->bindValue('firstname', $firstname, PDO::PARAM_STR);
            $sth->bindValue('lastname', $lastname, PDO::PARAM_STR);
            $sth->bindValue('email', $email, PDO::PARAM_STR);
            $sth->bindValue('password', $password, PDO::PARAM_STR);
            $sth->bindValue('role', $role, PDO::PARAM_STR);
            $sth->bindValue('status', $status, PDO::PARAM_INT);
            $sth->bindValue('avatar', $avatar, PDO::PARAM_STR);
            $sth->bindValue('bio', $bio, PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    /** Supprime un user
     * @param int $id identifiant de l'utilisateur
     */
    public function delete(int $id)
    {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('DELETE FROM user 
            WHERE u_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, PDO::PARAM_INT);

            //4. Executer ma requête
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }
}