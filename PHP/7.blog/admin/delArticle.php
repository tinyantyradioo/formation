<?php
session_start();
/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model Article */
require('../models/Article.php');

userIsConnected();

$view = '';
$pageTitle = '';


// Si le jeton (token) n'est pas le bon !
if (!isset($_POST['token']) || !isset($_SESSION['token']) || $_POST['token'] != $_SESSION['token']) {
    addFlashBag('Un vilain a essayé de te faire supprimer un truc ! Attention à toi !', 'success');
    header('Location: listArticle.php');
    exit();
}



if(isset($_POST['id'])) {
    $id = $_POST['id'];

    // Création d'une instance du model Category
    $articleModel = new Article();

    // On récupère la catégory
    $article = $articleModel->getById($id);

    // Si l'article existe bien dans la base
    if (!empty($article)) {

        // Execution de la requête de suppression
        $articleModel->delete($id);

        // Et il nous faut supprimer la photo éventuelle attachée à la catégorie
        deleteFile('../uploads/article/'. $article['a_picture']);

        addFlashBag('L\'article a bien été supprimée !','success');
    }
    else {
        addFlashBag('L\'article n\'existe pas !','warning');
    }
}
else {
    addFlashBag('Une erreur d\'accès à la page a eu lieu !', 'warning');
}

header('Location: listArticle.php');
exit();

