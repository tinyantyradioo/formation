<?php
session_start();

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model Catégorie et Article */
require('../models/Category.php');
require('../models/Article.php');

userIsConnected();

$view = 'dashboard';
$pageTitle = 'Dashboard';

// On récupère les élements nécessaire à la page

// On a besoin d'une instance du model ARTICLE
$articleModel = new Article();

// On a besoin d'une instance du model CATEGORY
$categoryModel = new Category();

// On va avoir besoin de la liste de toutes les catégories pour l'jout rapide d'un article
$categories = $categoryModel->getAll();

// Je vais avoir besoin d'une date de publication cachée pour l'article rapide
$createdAt = new DateTime('now',new DateTimeZone('Europe/Paris'));

//On récupère les 5 derniers articles créés
$articles = $articleModel->getAll(5,0,['a_created_at'=>'DESC']);

// Temporisation pour les commentaires
$comments = [];

/** Appel à la vue pour générer l'HTML */
include('views/layout.phtml');
