<?php
session_start();

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model User */
require('../models/User.php');

userIsConnected('ROLE_ADMIN');

/** Les variables nécessaire pour le Layout */
$view = 'user/add';
$pageTitle = 'Ajout d\'un utilisateur';

// Mode édition ou ajout ? A t-on un id dans l'URL
$id = (!empty($_GET['id'])) ? $_GET['id'] : null;


/** Les erreurs eventuelles lors de la récupération du formulaire */
$errors = [];

// On a besoin d'une instance du model USER
$userModel = new User();

/** Initialisation des variables du formulaire pour injecter les données */
if (!empty($id)) {
    $user = $userModel->getById($id);

    $firstname = $user['u_firstname'];
    $lastname = $user['u_lastname'];
    $email = $user['u_email'];
    $role = $user['u_role'];
    $bio = $user['u_bio'];
    $valid = $user['u_status'];
    $avatar = $user['u_avatar'];
} else {

    /** Initialisation des variables du formulaire pour injecter les données */
    $firstname = '';
    $lastname = '';
    $email = '';
    $role = '';
    $bio = '';
    $valid = false;
    $avatar = null;
}


//var_dump($_POST);

/** Si mon formulaire est posté ! */
if(isset($_POST['email'])) {

    // On vérifie si l'ID est transmis / mode édition
    $id = (!empty($_POST['id'])) ? $_POST['id'] : null;

    // On remplie les variables pour récupérer le contenu des champs et les réinjecter si nécessaire dans le formulaire
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $passwordConf = $_POST['passwordConf'];
    $role = $_POST['role'];
    $bio = $_POST['bio'];
    $valid = (isset($_POST['valid']))?true:false;
    $avatarCurrent = (isset($_POST['avatarCurrent'])) ? $_POST['avatarCurrent'] : null;

    /** On teste s'il y a des erreurs sur les champs du formulaire  */

    // Validation du Champ email
    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $errors['email'] = true;
    }

    // L'email est-il déjà dans la base ??
    // On ne peut pas avoir 2 emails identique !
    if(!empty($userModel->getByEmail($email, $id))) {
        $errors['emailDuplicate'] = true;
    }

    // validation du champ firstname
    if(empty($firstname) || strlen($firstname) < 3) {
        $errors['firstname'] = true;
    }

    // validation du champ lastname
    if (empty($lastname) || strlen($lastname) < 3) {
        $errors['lastname'] = true;
    }

    // validation du Champ password
    if(empty($password) || strlen($password) < 8 || $password != $passwordConf) {
        $errors['password'] = true;
    }

    // validation du Champ ROLES 
    if(empty($role) || !array_key_exists($role, ROLES)) {
        $errors['role'] = true;
    }

    // UPLOADER L'IMAGE 
    if (!empty($_FILES['avatar']['name']) && empty($errors)) {
        $avatar = uploadFile('avatar', 'user');
        if (empty($avatar))
            $errors['avatar'] = 'Une erreur d\'upload a eu lieu !';
        else {
            if (!empty($avatarCurrent)) {
                //si on a uploadé une image et qu'il y avait déjà une image (mode édition) : on supprime l'ancienne
                deleteFile(UPLOADS_DIR . 'user/' . $avatarCurrent);
            }
        }
    } else {
        $avatar = $avatarCurrent;
    }

    // Enfin s'il n'y a pas d'erreur on enregistre dans la base :
    if(empty($errors)) {

        // On hash le mot de passe 
        $password = password_hash($password, PASSWORD_DEFAULT);

        // Si on a pas d'ID : ajout
        if (empty($id)) {

            //Date de création 
            $createdAt = new DateTime('now', new DateTimeZone('Europe/Paris'));

            // On ajoute l'utilisateur
            $userModel->add($lastname, $firstname, $email, $password, $createdAt, $valid, $avatar, $bio, $role);
            
            //Ajout du message flash
            addFlashBag('L\'utilisateur a bien été ajoutée !', 'success');
        }
        else {
            //Ajout du message flash
            addFlashBag('L\'utilisateur a bien été modifée !', 'success');
            // mon modifie la catégorie !
            $userModel->update($id, $lastname, $firstname, $email, $password, $valid, $avatar, $bio, $role);
        
        }
        // On redirige vers la liste
        header('Location: listUser.php');
        exit();
    }

}

//var_dump($errors);
require('views/layout.phtml');