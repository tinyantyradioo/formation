<?php
session_start();
/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model User , Article */
require('../models/User.php');
require('../models/Article.php');

userIsConnected();

$view = '';
$pageTitle = '';


// Si le jeton (token) n'est pas le bon !
if (!isset($_POST['token']) || !isset($_SESSION['token']) || $_POST['token'] != $_SESSION['token']) {
    addFlashBag('Un vilain a essayé de te faire supprimer un truc ! Attention à toi !', 'success');
    header('Location: listArticle.php');
    exit();
}



if(isset($_POST['id'])) {
    $id = $_POST['id'];

    // Création d'une instance du model User
    $userModel = new User();
    // Création d'une instance du model User
    $articleModel = new Article();


    /** On va vérifier si l'utilisateur n'est pas rattachée à un article ! 
     * Sinon on ne peut pas le supprimer !
     */
    $articles = $articleModel->getByUserId($id);
    if (!empty($articles)) {
        addFlashBag('L\'utilisateur ne peut pas être supprimé. Un ou plusieurs articles y sont rattachés !', 'warning');
        header('Location: listuser.php');
        exit();
    }

    // On récupère l'utilisateur
    $user = $userModel->getById($id);

    /** On va vérifier que ce n'est pas l'utilisateur courant
     * Sinon on ne peut pas le supprimer !
     */
    if($_SESSION['user']['u_id'] == $user['u_id']) {
        addFlashBag('L\'utilisateur ne peut pas être supprimé. Vous êtes actuellement connecté !', 'warning');
        header('Location: listuser.php');
        exit();
    }

    

    // Si l'article existe bien dans la base
    if (!empty($user)) {

        // Execution de la requête de suppression
        $userModel->delete($id);

        // Et il nous faut supprimer la photo éventuelle attachée à la catégorie
        deleteFile(UPLOADS_DIR.'user/'. $user['u_avatar']);

        addFlashBag('L\'utilisateur a bien été supprimée !','success');
    }
    else {
        addFlashBag('L\'utilisateur n\'existe pas !','warning');
    }
}
else {
    addFlashBag('Une erreur d\'accès à la page a eu lieu !', 'warning');
}

header('Location: listUser.php');
exit();

