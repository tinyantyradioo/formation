<?php
session_start();

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model User */
require('../models/Category.php');

userIsConnected();

$view = 'category/list';
$pageTitle = 'Liste des catégories';


// Création d'une instance du model Category
$categoryModel = new Category();
$categories = $categoryModel->getAll();

// Récupère les données du flashbag
$flashbag = getFlashBag();

//Création d'un token de sécurité ! 
$token = bin2hex(openssl_random_pseudo_bytes(16));
// on écrit dans la session le token
// la prochaine requête POST devra contenir ce token ! 
$_SESSION['token'] = $token;


//var_dump($categories);
/** Appel à la vue pour générer l'HTML */
include('views/layout.phtml');
