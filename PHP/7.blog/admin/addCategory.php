<?php
session_start();

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model Catégorie */
require('../models/Category.php');

userIsConnected();

// Mode édition ou ajout ?
$id = (!empty($_GET['id']))? $_GET['id']:null;


/** Les variables nécessaire pour le Layout */
$view = 'category/add';
$pageTitle = (empty($id))?'Ajout d\'une catégorie':'Modifier la catégorie';


/** Les erreurs eventuelles lors de la récupération du formulaire */
$errors = [];

// On a besoin d'une instance du model CATEGORIE
$categoryModel = new Category();

var_dump($_FILES);

if(!empty($id)) {
    $category = $categoryModel->getById($id);
    $title = $category['c_title'];
    $description = $category['c_description'];
    $pictureCurrent = $category['c_picture'];
    $slug = $category['c_slug'];
}
else {

    /** Initialisation des variables du formulaire pour injecter les données */
    $title = '';
    $description = '';
    $picture = null;
    $pictureCurrent = null;
    $slug = '';
}


/** Si mon formulaire est posté ! */
if(isset($_POST['title'])) {
    // On vérifie si l'ID est transmis / mode édition
    $id = (!empty($_POST['id'])) ? $_POST['id'] : null;

    // On remplie les variables pour récupérer le contenu des champs et les réinjecter si nécessaire dans le formulaire
    $title = $_POST['title'];
    $description = $_POST['description'];
    $slug = strtolower($_POST['slug']);
    $picture = null;
    $pictureCurrent = (isset($_POST['pictureCurrent']))? $_POST['pictureCurrent'] : null;

    /** On teste s'il y a des erreurs sur les champs du formulaire  */

    // validation du champ title
    if(empty($title) || strlen($title) < 3) {
        $errors['title'] = true;
    }

    // validation du champ slug
    if (empty($slug) || strlen($slug) < 3 || !ctype_alnum(str_replace('-','',$slug))) {
        $errors['slug'] = true;
    }

    if(!empty($categoryModel->getBySlug($slug, $id))) {
        $errors['slug2'] = true;
    }

    // UPLOADER L'IMAGE 
    if(!empty($_FILES['picture']['name'])) {
        $picture = uploadFile('picture', 'category');
        if(empty($picture))
            $errors['picture'] = 'Une erreur d\'upload a eu lieu !';
        else {
            if(!empty($pictureCurrent)) { 
                //si on a uploadé une image et qu'il y avait déjà une image (mode édition) : on supprime l'ancienne
                deleteFile(UPLOADS_DIR . 'category/' . $pictureCurrent);
            }
        }
    }
    else {
        $picture = $pictureCurrent;
    }


    // Enfin s'il n'y a pas d'erreur on enregistre dans la base :
    if(empty($errors)) {

        $createdAt = new DateTime('now', new DateTimeZone('Europe/Paris'));

        if(empty($id)) {
            //Ajout du message flash
            addFlashBag('La catégorie a bien été ajoutée !', 'success');
            // On ajoute la catégorie
            $categoryModel->add($title, $createdAt, $slug, $description, $picture);
        }
        else {
            //Ajout du message flash
            addFlashBag('La catégorie a bien été modifée !', 'success');
            // mon modifie la catégorie !
            $categoryModel->update($id,$title,$slug,$description,$picture);
        }

       

        // On redirige vers la liste des catégories
        header('Location: listCategory.php');
        exit();
    }

}

//var_dump($errors);
require('views/layout.phtml');