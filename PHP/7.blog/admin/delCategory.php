<?php
session_start();
/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model Catégory et le model Article */
require('../models/Category.php');
require('../models/Article.php');

userIsConnected();

$view = '';
$pageTitle = '';


// Si le jeton (token) n'est pas le bon !
if (!isset($_POST['token']) || !isset($_SESSION['token']) || $_POST['token'] != $_SESSION['token']) {
    addFlashBag('Un vilain a essayé de te faire supprimer un truc ! Attention à toi !', 'success');
    header('Location: listCategory.php');
    exit();
}



if(isset($_POST['id'])) {
    $id = $_POST['id'];

    /** On va vérifier si la catégorie n'est pas rattachée à un article ! 
     * Sinon on ne peut pas la supprimer !
     */
    $articleModel = new Article();
    $articles = $articleModel->getByCategoryId($id);
    if(!empty($articles)) {
        addFlashBag('La catégorie ne peut pas être supprimée. Un ou plusieurs articles y sont rattachés !', 'warning');
        header('Location: listCategory.php');
        exit();
    }

    // Création d'une instance du model Category
    $categoryModel = new Category();

    // On récupère la catégory
    $category = $categoryModel->getById($id);

    // Si la catégorie existe bien dans la base
    if (!empty($category)) {
       
        // Execution de la requête de suppression
        $categoryModel->delete($id);

        // Et il nous faut supprimer la photo éventuelle attachée à la catégorie
        deleteFile(UPLOADS_DIR.'category/'.$category['c_picture']);

        // On ajoute un message flash !
        addFlashBag('La catégorie a bien été supprimée !','success');
    }
    else {
        addFlashBag('La catégorie n\'existe pas !','warning');
    }
}
else {
    addFlashBag('Une erreur d\'accès à la page a eu lieu !', 'warning');
}

header('Location: listCategory.php');
exit();

