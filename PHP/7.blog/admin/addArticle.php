<?php
session_start();

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model Catégorie et Article */
require('../models/Category.php');
require('../models/Article.php');

userIsConnected();

/** Les variables nécessaire pour le Layout */
$view = 'article/add';
$pageTitle = (empty($id)) ? 'Ajout d\'un article' : 'Modifier l\'article';



// Mode édition ou ajout ? A t-on un id dans l'URL
$id = (!empty($_GET['id'])) ? $_GET['id'] : null;


/** Les erreurs eventuelles lors de la récupération du formulaire */
$errors = [];

// On a besoin d'une instance du model ARTICLE
$articleModel = new Article();

// On a besoin d'une instance du model CATEGORY
$categoryModel = new Category();

// On va avoir besoin de la liste de toutes les catégories
$categories = $categoryModel->getAll();


if (!empty($id)) {
    $article = $articleModel->getById($id);

    $title = $article['a_title'];
    $content = $article['a_content'];
    $picture = $article['a_picture'];
    $slug = $article['a_slug'];
    $publishedAt = new Datetime($article['a_published_at']);
    $category = $article['category_c_id'];
    $user = $article['user_u_id'];
} else {

    /** Initialisation des variables du formulaire pour injecter les données */
    $title = '';
    $content = '';
    $picture = null;
    $slug = '';
    $publishedAt = new Datetime('now', new DateTimeZone('Europe/Paris'));
    $category = null;
    $user = null;
}


/** Si mon formulaire est posté ! */
if (isset($_POST['title'])) {

    // On vérifie si l'ID est transmis / mode édition
    $id = (!empty($_POST['id'])) ? $_POST['id'] : null;

    // On remplie les variables pour récupérer le contenu des champs et les réinjecter si nécessaire dans le formulaire
    $title = $_POST['title'];
    $content = $_POST['content'];
    $slug = strtolower($_POST['slug']);
    $date = $_POST['date'];
    $time = $_POST['time'];
    $category = $_POST['category'];
    $user = $_SESSION['user']['u_id'];
    $picture = null;
    $pictureCurrent = (isset($_POST['pictureCurrent'])) ? $_POST['pictureCurrent'] : null;

    /** On teste s'il y a des erreurs sur les champs du formulaire  */

    // validation du champ title
    if (empty($title) || strlen($title) < 3) {
        $errors['title'] = true;
    }

    // validation du champ date
    if (empty($date)) {
        $errors['date'] = true;
    }
    if (empty($time)) {
        $errors['time'] = true;
    }

    // validation du champ slug
    if (empty($slug) || strlen($slug) < 3 || !ctype_alnum(str_replace('-', '', $slug))) {
        $errors['slug'] = true;
    }

    if (!empty($categoryModel->getBySlug($slug, $id))) {
        $errors['slug2'] = true;
    }

    // UPLOADER L'IMAGE 
    if (!empty($_FILES['picture']['name']) && empty($errors)) {
        $picture = uploadFile('picture', 'article');
        if (empty($picture))
            $errors['picture'] = 'Une erreur d\'upload a eu lieu !';
        else {
            if (!empty($pictureCurrent)) {
                //si on a uploadé une image et qu'il y avait déjà une image (mode édition) : on supprime l'ancienne
                deleteFile(UPLOADS_DIR . 'article/' . $pictureCurrent);
            }
        }
    } else {
        $picture = $pictureCurrent;
    }


    // Enfin s'il n'y a pas d'erreur on enregistre dans la base :
    if (empty($errors)) {
        // Si on a une date transmise sinon la date de publication est mise à la date du jour
      
        $publishedAt = new Datetime($date . ' ' . $time, new DateTimeZone('Europe/Paris'));
       

        // Si on a pas d'ID : ajout
        if (empty($id)) {

            //Date de création 
            $createdAt = new DateTime('now', new DateTimeZone('Europe/Paris'));

            //Ajout du message flash
            addFlashBag('L\'article a bien été ajoutée !', 'success');

            // On ajoute la catégorie
            $articleModel->add($title, $createdAt, $publishedAt, $slug, $category, $user, $content, $picture);
        } else {
            // Sinon on modifie !

            //Date de création 
            $modifiedAt = new DateTime('now', new DateTimeZone('Europe/Paris'));
            //Ajout du message flash
            addFlashBag('L\'article a bien été modifée !', 'success');
            // mon modifie la catégorie !
            $articleModel->update($id, $title, $modifiedAt, $publishedAt, $slug, $category, $user, $content, $picture);
        }


        
        // On redirige vers la liste des catégories
        header('Location: listArticle.php');
        exit();
    }
}

//var_dump($errors);
require('views/layout.phtml');
