<?php
session_start();

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Inclure le model User */
require('../models/User.php');


// Création du modèle user
$userModel = new User();


// Variable pour réinjecter dans la formulaire
$email = '';

//Les erreurs
$errors = [];

//$userModel->getByEmail()

//var_dump($_POST);
//Si l'utilisateur est connecté on le redirige vers la page d'acuueil !
if(isset($_SESSION['connected']) && $_SESSION['connected'] == true) {
    header('Location: index.php');
    exit();
}

//Si donnees LOGIN transmises
if(isset($_POST['email'])) {

    $email = $_POST['email'];
    $passsword = $_POST['password'];

    //Récupérer l'utilisateur avec l'email dans la base de donnée
    $user = $userModel->getByEmail($email);

    //Si l'utilisateur existe dans la base
    if(!empty($user)) {
        //On vérifie si le mot de passe est bon 
        if(password_verify($passsword, $user['u_password'])) {
            //On écrit dans la session
            $_SESSION['connected'] = true;
            // Je retire le mot de passe avant de stocker le user dans la session (pas besoin !)
            unset($user['password']);
            $_SESSION['user'] = $user;

            header('Location: index.php');
            exit();
        } else {
            $errors['email'] = 'Nom utilisateur ou mot de passe incorrects !';
        }
    }
    else {
        $errors['email'] = 'Nom utilisateur ou mot de passe incorrects !';
    }
}

include('views/login.phtml');

/**
 * 
 * Si donnees LOGIN transmises
 *  
 *  Récupérer l'uilisateur avec l'email dans la base
 *  Si l'utilisateur existe dans la base
 *      On vérifie si le mot de passe est bon 
 *      Si il est bon 
 *          On écrit dans la session
 *              qu'il est connecté
 *              et ses donnees user (id, nom...)
 *          Redirige vers l'accueil du back
 *      Sinon 
 *          erreurs à afficher dans le formulaire
 * sinon
 *      erreur à afficher dans le formulaire
 * fin Si
 * 
 * renvoie la vue (initiale ou avec erreur).
 */

