<?php

use Faker\Factory;
use App\model\Salon;
use App\model\User;
use App\model\Message;
use App\classe\Database;


/** Autoload de Composer */
require_once 'vendor/autoload.php';

/** la config */
require('config/config.php');




/** Le programme */

/** On instancie Fake */
$faker = Factory::create('fr_FR');

/** On instancie nos modèles */
$salonModel = new Salon();
$userModel = new User();
$messageModel = new Message();

/** On supprime les élements présent dans les tables */
$database = new Database();
$database->execute('DELETE FROM message');
$database->execute('DELETE FROM user');
$database->execute('DELETE FROM salon');

/** On va créer les données Fake dans la base de données */
for ($i =1;$i<=3;$i++) {
    $salonId = $salonModel->add($faker->word(), new \DateTime(), $faker->slug(3), $faker->realText(200));

    for($j=1;$j<=4;$j++) {
       $userId = $userModel->add($faker->userName(),$faker->firstName(),
        $faker->lastName(),
         $faker->email(),
          new DateTime(),
           $faker->password(),
            $faker->imageUrl(300, 300, 'cat'),
             'STATUS_ONLINE',null); 

             for($k=1;$k<=4;$k++) {
                $messageid = $messageModel->add($faker->paragraph(),new \DateTime(), $salonId, $userId);
            }
    }
}