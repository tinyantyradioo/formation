<?php
/*
Un exemple d'autoload en PSR-4 ...
Mais on utilise l'autoload de Composer à la place !
*/

//spl_autoload_register('autoloadFab');


function autoloadFab($class) {
    var_dump($class);

    $class = str_replace(['App\\','\\'],['src/','/'], $class);

    if(file_exists(DIR.$class.'.php'))
        require(DIR .$class . '.php');
    //var_dump(DIR .$class . '.php');
    /* if(file_exists('src/classe/'.$class.'.php'))
        require('src/classe/' . $class . '.php');

    elseif(file_exists('src/model/'.$class.'.php'))
        require('src/model/' . $class . '.php'); */
}
