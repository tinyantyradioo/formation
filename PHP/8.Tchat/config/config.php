<?php
/** Default values */
const DEFAULT_CONTROLLER = 'home';

/** Mode déboguage activé ! */
const DEBUG = true;

/** Les rôles dispo sur le backOffice de mon blog */
const ROLES = ['ROLE_ADMIN'=>'Administrateur','ROLE_AUTH'=>'Auteur'];


const URL = 'http://localhost/ADF/php/8.Tchat/';
const DIR = 'C:\wamp64\www\formation\PHP\8.Tchat\\';

/** LE DOSSIER D'UPLOAD PAR DEFAUT ! */
const UPLOADS_DIR = DIR.'uploads\\';
/** L'URL D'UPLOAD PAR DEFAUT ! */
const UPLOADS_URL = URL.'uploads/';

/** Fichiers images valide à lupload */
const UPLOADS_VALIDE_IMG = ['jpg'=> 'image/jpeg', 'png'=> 'image/png','gif'=> 'image/gif', 'webp'=> 'image/webp', 'jpeg'=> 'image/jpeg'];
 
/** CONNEXION BDD */
const DB_DSN = 'mysql:dbname=tchat;host=localhost;charset=UTF8';
const DB_USER = 'root';
const DB_PASSWORD = '';


