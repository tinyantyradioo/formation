<?php

namespace App\model;

use App\classe\Database;
use DateTime;


/**
 * Message
 */

 class Message{
     
    /** 
     * @var \Database $database un objet Database (singleton connexion + méthode requête)
     */
    private $database;

    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->database = new Database();
    }
    
    /** Retourne toute les messages
     * @return array|false le jeu d'enregistrement ou false si une erreur survient
     */
    public function getAll()
    {
        try {

            return $this->database->select('SELECT * FROM message');

        } catch (\PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }

    }

        /** Retourne tout les messages
     * @param int $id identfiant du message
     * @return array|false le jeu d'enregistrement ou false si une erreur survient
     */
    public function getById(int $id)
    {
        try {

            return $this->database->selectOne('SELECT * FROM message WHERE m_id = :id', ['id'=>$id]);

        } catch (\PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }
    }

    /**
     * add
     *
     * @param  string $message
     * @param  DateTime $createdAt
     * @param  int $salonId
     * @param  int $userId
     * @return void
     */
    public function add(string $message, \DateTime $createdAt, int $salonId, int $userId) {
        try {

          return $this->database->execute('INSERT INTO message (m_message, m_created_at, salon_id, user_id) 
          VALUES (:message, :createdAt, :salon_id, :user_id)', ['message'=> $message, 'createdAt'=> $createdAt->format('Y-m-d H:i'),  'salon_id'=> $salonId, 'user_id'=> $userId] );

        } catch (\PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }




    

 }
 