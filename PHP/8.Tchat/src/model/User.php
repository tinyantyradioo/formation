<?php

namespace App\model;

use App\classe\Database;

/**
 * user
 */
class User {
    
    /** 
     * @var \Database $database un objet Database (singleton connexion + méthode requête)
     */
    private $database;

    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->database = new Database();
    }

    /** Retourne toutes les catégories
     * @return array|false le jeu d'enregistrement ou false si une erreur survient
     */
    public function getAll()
    {
        try {

            return $this->database->select('SELECT * FROM user');

        } catch (\PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }

    }

    /** Retourne toutes les catégories
     * @param int $id identfiant de la catégorie
     * @return array|false le jeu d'enregistrement ou false si une erreur survient
     */
    public function getById(int $id)
    {
        try {

            return $this->database->selectOne('SELECT * FROM user WHERE u_id = :id', ['id'=>$id]);

        } catch (\PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }
    }

    /** Retourne un user par rapport à son email
     * @param string $email le email recherché dans la table user
     */
    public function getByEmail(string $email, int $excludeId=null) {
        try {

            return $this->database->selectOne('SELECT u_email FROM user WHERE u_email = :email AND u_id != :id', ['id' => $excludeId, 'email'=> $email]);

        } catch (\PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }



    /**
     * add
     *
     * @param  string $name
     * @param  DateTime $createdAt
     * @param  string $slug
     * @param  string $description
     * @return void
     */
    public function add(string $username, string $firstName, string $lastName, string $email, \DateTime $createdAt, string $password, ?string $avatar = null, string $status = 'STATUS_ONLINE') {
        try {

          return $this->database->execute('INSERT INTO user (u_username, u_firstname, u_lastname, u_email,  u_created_At, u_password, u_avatar, u_status, u_connected_at) VALUES (:username,:firstName, :lastName, :email, :createdAt, :password, :avatar, :status, null )', ['username'=> $username, 'firstName'=> $firstName, 'lastName' => $lastName, 'email' => $email, 'createdAt'=>$createdAt->format('Y-m-d H:i'), 'password'=> $password, 'avatar'=> $avatar, 'status'=>$status] );

        } catch (\PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    /**
     * modification
     ** @param  int $id
     * @param  string $title
     * @param  string $slug
     * @param  string $picture
     * @param  string $description
     * @return void
     */
    public function update(int $id, string $title, string $slug, ?string $description = null, ?string $picture = null)
    {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('UPDATE category 
            SET c_title=:title, c_description=:description, c_picture=:picture, c_slug=:slug
            WHERE c_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, \PDO::PARAM_INT);
            $sth->bindValue('title', $title, \PDO::PARAM_STR);
            $sth->bindValue('description', $description, \PDO::PARAM_STR);
            $sth->bindValue('picture', $picture, \PDO::PARAM_STR);
            $sth->bindValue('slug', $slug, \PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();
        } catch (\PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    /** Supprime une catégory
     * @param int $id identifiant de la catégorie
     */
    public function delete(int $id) {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('DELETE FROM category 
            WHERE c_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, \PDO::PARAM_INT);

            //4. Executer ma requête
            $sth->execute();
        } catch (\PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }
}