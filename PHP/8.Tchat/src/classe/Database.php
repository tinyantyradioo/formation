<?php

namespace App\classe;

/* PSEUDO SINGLETON
C'est la propriété DBH qui ne sera instanciée 
qu'une seule fois ! 
*/

class Database
{
    protected static $dbh;

    public function __construct()
    {
        // echo 'Construit !';
        if (!self::$dbh instanceof \PDO) {
            // echo 'Creation connexion PDO';
            self::$dbh = new \PDO(DB_DSN, DB_USER, DB_PASSWORD);
            self::$dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            self::$dbh->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            // var_dump(self::$dbh);
        }
    }

    public function selectOne(string $sql, array $bindValues = array())
    {
        $sth = self::$dbh->prepare($sql);
        /*foreach ($bindValues as $bindValue)
            $sth->bindValue($bindValue[0], $bindValue[1], $bindValue[2]);*/
        $sth->execute($bindValues);

        return $sth->fetch();
    }

    function select(string $sql, array $bindValues = array())
    {
        $sth = self::$dbh->prepare($sql);
        /*foreach ($bindValues as $bindValue)
            $sth->bindValue($bindValue[0], $bindValue[1]); //, $bindValue[2]*/
        $sth->execute($bindValues);

        return $sth->fetchAll();
    }

    function execute(string $sql, array $bindValues = array())
    {
        $sth = self::$dbh->prepare($sql);
        /*foreach ($bindValues as $bindValue)
            $sth->bindValue($bindValue[0], $bindValue[1]); //, $bindValue[2]*/
        $sth->execute($bindValues);

        return self::$dbh->lastInsertId();
    }
}
