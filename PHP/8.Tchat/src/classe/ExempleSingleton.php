<?php

namespace App\classe;

/** Un exemple de SINGLETON COMPLET
* Cette classe ne peut-être instanciée qu'une seules fois !
*/
class ExempleSingleton {

    /** L'instance de l'objet */
    private static $instance = null;


    private function __construct() {
        echo 'je suis créé !';
    }

    public static function create() {
        if(self::$instance == null)
            self::$instance = new ExempleSingleton();

        return self::$instance;
    }

}