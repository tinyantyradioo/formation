<?php

use Faker\Factory;

require_once 'vendor/autoload.php';


// use the factory to create a Faker\Generator instance
$faker = Factory::create('fr_FR');
// generate data by calling methods
echo $faker->name().'<br>';
// 'Vince Sporer'
echo $faker->email() . '<br>';
// 'walter.sophia@hotmail.com'
echo $faker->realText() . '<br>';
// 'Numquam ut mollitia at consequuntur inventore dolorem.'
echo $faker->userName();
