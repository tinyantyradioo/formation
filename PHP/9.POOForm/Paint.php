<?php

require_once "classes/form2d.class.php";
require_once "classes/Rectangle.class.php";
require_once "classes/Square.class.php";
require_once "classes/Circle.class.php";
require_once "classes/Ellipse.class.php";
require_once "classes/Polygone.class.php";


class Paint {

    /**
     * @var array la liste des formes présente
     */
    private array $formes;


    public function __construct() {
        $this->formes = [];
    }


    public function run() {

        foreach($this->formes as $forme) {
           $this->forme = $forme->draw();
        }

        

        
    }


    /**
     * Get la liste des formes présent
     *
     * @return  array
     */ 
    public function getFormes()
    {
        return $this->formes;
    }

    /**
     * Ajoute une forme à la liste
     *
     * @param  Forme2d  
     *
     * @return  self
     */ 
    public function addForme(Forme2d $forme)
    {
        $this->formes[] = $forme;

        return $this;
    }
}