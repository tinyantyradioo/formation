<?php



// les classes définies comme abstraites ne peuvent pas être instanciées
abstract class Form2d
{
    /**
     * @var int position 
     * $x
     */
    private int $x;
    /**
     * @var int position 
     * $y
     */
    private int $y;


    /**
     * @var int hauteur de l'objet
     * $width
     */
    private int $width;

        /**
     * @var int largeur de l'objet
     * $height
     */
    private int $height;
    // private string $style;

    /**
     * @var string couleur de l'element
     *  $fillColor
     */
    private string $fillColor;

    /**
     * @var string couleur de la bordure
     *  $strokeColor
     */
    private string $strokeColor;

    /**
     * @var int taille de la bordure
     *  $strokeWidth
     */
    private int $strokeWidth;

        /**
     * @var float intensité de l'opacité de la bordure
     *  $opacity
     */
    private float $opacity;


    /**
     * __constructeur
     *
     * @return void
     */
    public function __construct()
    {
        $this->x = 0;
        $this->y = 0;
        // $this->width = 150;
        // $this->height = 150;
        // $this->style = "fill:blue;stroke:pink;stroke-width:5;opacity:0.5";
        $this->fillColor = 'blue';
        $this->strokeColor = 'pink';
        $this->strokeWidth = 5; 
        $this->opacity = 0.5;
    }


    // ************************************************

    /**
     * * getter de la propriété $x
     * @return int $x
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * setter de la propriété $x
     * @param int $x
     * @return void
     */
    public function setX($x): void
    {
        $this->x = $x;
    }

    // ************************************************

    /**
     * * getter de la propriété $y
     * @return int $y
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * setter de la propriété $y
     * @param int $y
     * @return void
     */
    public function setY($y): void
    {
        $this->y = $y;
    }

    // ************************************************

    /**
     * * getter de la propriété $width
     * @return int $width
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * setter de la propriété $width
     * @param int $width
     * @return void
     */
    public function setWidth($width): void
    {
        $this->width = $width;
    }

    // ************************************************

    /**
     * * getter de la propriété $height
     * @return int $height
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * setter de la propriété $height
     * @param int $height
     * @return void
     */
    public function setHeight($height): void
    {
        $this->width = $height;
    }

    // ************************************************

    // public function getStyle(): string
    // {
    //     return $this->style;
    // }

    // public function setStyle(string $colorFill, string $colorStroke, int $widthStroke, float $opacity): void
    // {
    //     $this->style = "\'fill:$colorFill;stroke:$colorStroke;stroke-width:$widthStroke;opacity:$opacity";
    // }



    // return "<rect x='$this->x' y='$this->y width='$this->width' heght='$this->height' style=\'fill:blue;stroke:pink;stroke-width:5;opacity:0.5 />";

         // ************************************************

    /**
     * * getter de la propriété $fillColor
     * @return string $fillColor
     */
    public function getFillColor(): string
    {
        return $this->fillColor;
    }

    /**
     * setter de la propriété $fillColor
     * @param string $fillColor
     * @return void
     */
    public function setFillColor($fillColor): void
    {
        $this->fillColor = $fillColor;
    }   

             // ************************************************

    /**
     * * getter de la propriété $strokeColor
     * @return string $strokeColor
     */
    public function getStrokeColor(): string
    {
        return $this->strokeColor;
    }

    /**
     * setter de la propriété $strokeColor
     * @param string $strokeColor
     * @return void
     */
    public function setStrokeColor($strokeColor): void
    {
        $this->strokeColor = $strokeColor;
    }   

                 // ************************************************

    /**
     * * getter de la propriété $strokeWidth
     * @return int $strokeWidth
     */
    public function getStrokeWidth(): int
    {
        return $this->strokeWidth;
    }

    /**
     * setter de la propriété $strokeWidth
     * @param int $strokeWidth
     * @return void
     */
    public function setStrokeWitdh($strokeWidth): void
    {
        $this->strokeWidth = $strokeWidth;
    }   

                     // ************************************************

    /**
     * * getter de la propriété $opacity
     * @return float $opacity
     */
    public function getOpacity(): float
    {
        return $this->opacity;
    }

    /**
     * setter de la propriété $opacity
     * @param int $opacity
     * @return void
     */
    public function setOpacity($opacity): void
    {
        $this->opacity = $opacity;
    }   


    abstract public function draw(
        // $width, $height
    );
}
