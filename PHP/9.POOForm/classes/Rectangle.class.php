<?php
require_once "form2d.class.php";



class Rectangle extends Form2d
{

    /**
     * __constructeur
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function draw(): string
    {
        return
            // "<rect 
            // x='{$this->getX()}'
            // y='{$this->getY()}'
            // width='{$this->getWidth()}'
            // height='{$this->getHeight()}'
            // style='{$this->getStyle()}' /> ";
            "<rect x='{$this->getX()}' y='{$this->getY()}' width='190' height='150' style='fill:{$this->getFillColor()};stroke:{$this->getStrokeColor()};stroke-width:{$this->getStrokeWidth()};opacity:{$this->getOpacity()}'";
            
    }
    
}


// <rect x="50" y="20" width="150" height="150" style="fill:blue;stroke:pink;stroke-width:5;opacity:0.5" />