<?php
require_once "Rectangle.class.php";



class Square extends Rectangle
{
    
        /**
     * __constructeur
     *
     * @return void
     */
    public function __construct()
    {
        
        parent::__construct();
    }
    

    public function draw(): string
    {
        return

            "<rect x='{$this->getX()}' y='{$this->getY()}' width='100' height='100' style='fill:{$this->getFillColor()};stroke:{$this->getStrokeColor()};stroke-width:{$this->getStrokeWidth()};opacity:{$this->getOpacity()}'";
            
    }
}











