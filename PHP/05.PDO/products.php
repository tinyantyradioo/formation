<?php
/** On inclu la configuration */
require('config/config.php');

/** On inclu les librairies */
require('lib/db.php');

$view = 'products';

try {

    /** 1 : on se connecte au serveur MySQL*/
    $dbh = connect();

    /** 2 : on préparer notre requête ! */
    $sth = $dbh->prepare('SELECT * FROM products');

    /** 3 : on exécute la requête SQL ! */
    $sth->execute();

    /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
    $products = $sth->fetchAll();

    // var_dump($products);
    /** Appel à la vue pour générer l'HTML */
    include('tpl/layout.phtml');

}
catch(PDOException $e) {
    echo $e->getMessage();
    var_dump($e->getTrace());
}