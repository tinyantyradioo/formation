<?php

/** On inclu la configuration */
require('config/config.php');

/** On inclu les librairies */
require('lib/db.php');

var_dump($_GET);
echo $_GET['id'];

$view = 'orders';

try {

        /** 0.1 verifier les données, et utilise la clé de registre.   */
    if(isset($_GET['id']) && !empty($_GET['id'])) {
    
        /** 1 : on se connecte au serveur MySQL*/
        $dbh = connect();

        /** 2 : on préparer notre requête ! */
        $sth = $dbh->prepare('SELECT * FROM customers INNER JOIN orders ON (orders.customerNumber=customers.customerNumber) WHERE orderNumber= :id');

        /** 2.1 On va lier les jetons avec les valeurs  */
        $sth->bindValue(':id',$_GET['id'],PDO::PARAM_INT);


        /** 3 : on exécute la requête SQL ! */
        $sth->execute();

        /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
        $customer = $sth->fetch();

        //var_dump($customers);
        /** Appel à la vue pour générer l'HTML */
        // include('tpl/layout.phtml');

        var_dump($customer);

        /** etape bonus, preparer la 2eme requete */
        $sth = $dbh->prepare('SELECT *, (quantityOrdered * priceEach) AS Total
        FROM orderdetails 
        INNER JOIN products ON orderdetails.productCode = products.productCode
        WHERE orderNumber = :id');
        /** 2.1 On va lier les jetons avec les valeurs  */
        $sth->bindValue(':id',$_GET['id'],PDO::PARAM_INT);
        /**  : on exécute la requête SQL ! */
        $sth->execute();
        /** etape bonus, faire un fetchAll  */
        $bonDeCommandes = $sth->fetchAll();
        
        // 3eme requete calcul du total de chaque element du bon de commande
        $sth = $dbh->prepare('SELECT *, ROUND(sum(quantityOrdered * priceEach),2) AS superTotal
        FROM orderdetails 
        INNER JOIN products ON orderdetails.productCode = products.productCode
        WHERE orderNumber = :id');
        $sth->bindValue(':id', $_GET['id'], PDO::PARAM_INT);


        $sth->execute();
        $superTotal = $sth->fetch();



        /** etape bonus, Appel à la vue  */
        include('tpl/layout.phtml');
         var_dump($bonDeCommandes);
       
    }

    else {
        header('Location: index.php');
    }

}

catch(PDOException $e) {
    echo $e->getMessage();
    var_dump($e->getTrace());
}