<?php

/**
 * @var string Le nom de la vue de la page !
 */
$view = 'index';

/** 
 * @var Array On initialise le tableau de toutes les tâches
 */
$tasks = [];


// On ouvre le fichier CSV en mode lecture seule, pour y lire les tâches
$fp = fopen('tasks.csv','r');

// tant que il y a des lignes dans mon fichiers je les lis
while(($task = fgetcsv($fp)) != false) {
    // je récupère la ligne courante (le pointeur se place alors sur la ligne suivante du fichier)
    // fgetcsv me renvoi un tableau contenant toutes les colonnes présentent sur ma ligne CSV
    // et je place directement ce tableau dans le tableau qui doit contenir toutes les têches
    $tasks[] = $task;
}


// je débogue : si mon CSV est vide, j'aurais un tableau vide, sinon j'aurais un tableau rempli ne N tableau, correspondant au N ligne de mon fichier CSV
// var_dump($tasks);

// Je vais maintenant inclure la vue qui va faire le travail pour générer l'HTML
include('tpl/layout.phtml');