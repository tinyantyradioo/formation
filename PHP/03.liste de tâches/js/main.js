'use strict';

window.addEventListener("DOMContentLoaded", (event) => {
    console.log("DOM entièrement chargé et analysé");

    toggle_creation_form(); //function du toggle, creation d'une tache

    experimental_Target(); // target toute les tache, pour les modifier

    

})

function toggle_creation_form() {
    const btn = document.querySelector('#Ajouter_une_tache');
    const tasks = document.querySelector('#modal');
    console.log(btn);

    btn.addEventListener('click', test);


    function test() {
    tasks.classList.toggle("toggletest");
}}

function experimental_Target() {
    document.querySelector("ul").addEventListener("click", function (e) {
        console.log(e.target.textContent);
        console.log(this);
        // Clique cible une des tâches.
        const tasks = e.target.textContent;
        // #1 Récuperer la valeur de e.target ===> e.target.textContent "DONE"
        let targetTask = e.target.dataset;
        console.log (targetTask.title + targetTask.date + targetTask.description);

        document.write(targetTask.title + targetTask.date + targetTask.description);
        // <input type="text" placeholder="name of task" name='title'>
    })      
}    
       