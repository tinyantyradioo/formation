# Liste de tâches

> Une application pour gérer vos tâches.

Nous commençons à maîtriser un peu PHP. Mais nous n'avons pas encore moyen de stocker des données sur le serveur.

Avant d'apprendre à manipuler les bases de données nous allons apprendre à bien travailler avec PHP. Mais il va nous falloir trouver un moyen de sauvegarder nos tâches afin de construire une application "solide".

Pour cela nous allons utiliser la capacité de PHP à manipuler les fichiers sur le disque dur du serveur.

Et nous allons utiliser un format de fichier spécifique, le CSV (Comma separated values). Le format CSV est appelé un format d'échange. Il permet de représenter une feuille de calcul (type excel) sous la forme d'un fichier texte qui peut donc être échanger entre différentes plateformes (import/export de données).

> Exemple de fichier CSV contenant des tâches

    "Coder en PHP","2020-02-07 14:00","Finir tous les exercices pour ce soir","1"
    "Boire une bière","2020-02-07 18:00","Retrouver les copains au bar du coin pour partager une bière de l'amitié","3"

Ici nous avons 2 tâches, donc 2 lignes dans le fichier. Chaque ligne contient plusieurs colonnes qui sont séparées par des virgules.

## PHP et le CSV

PHP dispose nativement de fonctions permettant de manipuler des fichiers mais aussi particulièrement des fichiers CSV.

Le principe est simple :

    - On ouvre le fichier pour obtenir un accès (un pointer vers le fichier),
    - On choisi si on veut écrire, lire au début ou à la fin du fichier,
    - On écrit ou on lit une ligne ou tout le fichier ou un seul caractère, ou on peut directement interprêter la valeur d'une ligne (une ligne CSV par exemple)
    - On ferme le fichier quand on a fini

## Notre programme

Il doit nous permettre d'ajouter, modifier ou supprimer une tâche.
Nos tâches sont sauvegardées dans un fichier CSV.

A l'ouverture du programme nous retrouvons toutes les tâches disponibles sur la page.
Le titre des tâches s'affichent par ordre croisant (selon la date).

    - Une tâche avec une priorité urgente (3) apparait en rouge.
    - Une tâche avec une priorité normal (2) apparait en noir
    - Une tâche avec une priorité basse (1) apparait en vert

    - Une tâche en retard est mise en gras et elle est marquée "En retard".

Quand on clique sur une tâche on affiche le détail complet de la tâche sur la partie droite.

Dans le détails de la tâche on a la possibilité de supprimer ou de modifier la tâche en cliquant sur 2 boutons distincts.

En haut de page nous avons un boutons "+ Ajouter une tâche". Au clic sur ce bouton un formulaire apparait et permet de saisir une nouvelle tâche :

    - Nom de la tâche,
    - Date de la tâche (Jour, Mois, Année, Heure),
    - Description de la tâche,
    - Priorité de la tâche (Basse, Normale, Haute)

## Organisation

Ici le programme se complexifie un peu. Il va falloir nous organiser et découper le développment en plusieurs petites tâches. 
Nous allons utiliser du PHP, de l'HTML, du CSS, et même un petit peu de JS.
Il est donc impératif d'avoir en tête toute l'organisation de notre programme avant de commencer à développer. Sinon c'est la cata assurée !

> Etape 1 : le MOCKUP.

Créer un mockup de votre application à l'aide de balsamik ou de NinjaMok. Chaque étape doit ici être détaillée.

> Etape 2 : les fonctionnalités

Il nous faut détailler toutes les fonctionnalités de notre programme.
Exemple : ajout d'une tâche, suppression d'une tâche

> Etape 3 : détailler le processus de fonctionnement

Détailler chaque étape avec précision. Que doit-il se passer ? Y a t'il une requête vers le serveur ? Quelles sont toutes les étapes du programme quand il reçoit le requête ?

> Etape 4 : commencer à coder étape par étape

Il nous faut commencer à coder mais pas tout en même temps. Faire par exemple l'affichage des tâches pour commencer...

## Quelques conseils 

> Nous utiliserons plusieurs fichiers PHP. Un pour chacune des grandes fonctionnalités de notre applications (un fichier pour ajouter une tâche, un pour afficher les tâches, un pour supprimer...). Ces fichiers seront des fichiers indépendants qui effectuerons chacun une tâche bien précise. Ils s'appuierons sur des librairies de fonctions communes à tous les fichiers (exemple : lire toutes les tâches dans le fichier CSV)

> PHP avec sa fonction "include" permet de charger un fichier qui contient plusieurs fonctions (un fichier librairies de fonctions)

> Prenez le temps de tester les exemples de code pour utiliser les fonctions PHP que vous ne connaissez pas. Par exemple sur les fichiers. Faites un essaie d'ouverture, de lecture, de lecture CSV... Vous pouvez utiliser l'exemple CSV ci-dessus pour remplir un fichier et tester par exemple. 

> Bref il ne doit rester aucun doute dans votre tête avant de vous lancer dans le codage. Aucun doute sur les fonctionnalités et leur répartition, mais aussi aucun doute sur les besoins de fonction PHP et leur utilisation.



