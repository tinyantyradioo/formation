<?php

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');


$view = 'addUser';
$pageTitle = 'Ajout d\'un utilisateur';

/** Les erreurs eventuelles */
$errors = [];

/** Initialisation des variables du formulaire */
$firstname = '';
$email = '';

var_dump($_POST);

/** Si mon formulaire est posté ! */
if(isset($_POST['email'])) {

    $firstname = $_POST['firstname'];
    $email = $_POST['email'];

    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $errors['email'] = true;
    }

    if(empty($firstname) || strlen($firstname) < 3) {
        $errors['firstname'] = true;
    }


}

var_dump($errors);
require('views/layout.phtml');