<?php

/** Renvoi un nombre formaté en format monétaire ! 
 * @param float $number le nombre à convertir
 * @return string la chaine correspondant au nombre avec la devise
 */
function formatMoneyNumber(float $number) : string 
{
    return number_format($number, 2, ',', ' ').' '.MONEY_DEVISE;
}