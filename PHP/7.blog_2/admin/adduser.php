<?php

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');


$view = 'addUser';
$pageTitle = 'Ajout d\'un utilisateur';

/** Les erreurs eventuelles */
$errors = [];

/** Initialisation des variables du formulaire */
$firstname = '';
$email = '';
$passwordHash = '';

var_dump($_POST);

/** Si mon formulaire est posté ! */
if(isset($_POST['email'])) {

    $Firstname = $_POST['Firstname'];
    $email = $_POST['email'];
    $lastname = $_POST['lastname'];
    $password = $_POST['password'];
    $passwordConf = $_POST['passwordConf'];

    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $errors['email'] = true;
    }

    if(empty($Firstname) || strlen($Firstname) < 3) {
        $errors['Firstname'] = true;
    }

    if(empty($lastname) || strlen($lastname) < 3) {
        $errors['lastname'] = true;
    }

    if(empty($password) || strlen($password) < 3) {
        $errors['password'] = true;

        if(empty($passwordConf) || strlen($passwordConf) < 3) {
            $errors['passwordConf'] = true;
        }
    }

    if($_POST['password'] === $_POST['passwordConf'] && empty($errors)){
        $passwordHash = password_hash($password, PASSWORD_DEFAULT); 
    } else {
        $errors['passwordConf'] = true;
        $errors['password'] = true;
    }

    if (empty($errors)) {
        
    }

}

// if(empty($errors)) {
    
// }

var_dump($passwordHash);
var_dump($errors);
require('views/layout.phtml');