# Requête SQL

## Exemple
```sql
SELECT customerName, contactFirstName, contactLastName, country  	#les colonne sélectionnées dans la table
FROM customers														# le nom de la table
WHERE country = 'France' OR country = 'Germany'						# condition par rapport aux données présentent dans la table
ORDER BY country DESC, customerName ASC								# tri sur le jeu d'enregistrement 
```

## Documentation
http://sql.sh

## SELECT

> La liste des bureaux (adresse et ville) triés par pays décroissant puis par état

    RESULTAT ==> 7 lignes / 100 Market Street

    ```SQL
    SELECT addressLine1, addressLine2, city
    FROM offices
    ORDER BY country DESC
    ```

## WHERE 

> La liste de tous les produits qui sont de type Voiture (Car)

  RESULTATS ==> 62 Ligne / S10_1949 	1952 Alpine Renault 1300

    ```SQL
SELECT productLine, productName
FROM products
WHERE productLine = "Vintage Cars" OR productLine = "Classic Cars"
```

  

> La liste des avions (code et nom) triés par vendeur et par quantité en stock décroissants

    RESULTAT ==> 12 lignes / 1900s Vintage Tri-Plane

    ```SQL
        SELECT productLine, productName
        FROM products
        WHERE productLine = "Planes" 
        ORDER BY productVendor DESC, quantityInStock DESC
    ```   

> La liste des produits (code, nom, échelle et quantité) qui ont une échelle soit de 1:10, soit de 1:18 triés par quantité en stock décroissante

    RESULTAT ==> 48 lignes / 1995 Honda Civic

    ```SQL
        SELECT productCode, productName, productScale, quantityInStock
        FROM products
        WHERE productScale = "1:10" OR productScale = "1:18"
        ORDER BY quantityInStock DESC
    ```  

> La liste des produits (nom, vendeur et prix de vente) qui sont vendus au moins 132$ triés par nom du produit

    RESULTAT ==> 24 lignes / 1903 Ford Model A

    ```SQL
        SELECT productName, productVendor, MSRP
        FROM products
        WHERE MSRP>132 OR MSRP=132
        ORDER BY productName ASC
    ``` 

> La liste des produits (code, nom et prix d'achat) des produits achetés au moins 60$ au plus 90$ triés par prix d'achat

    RESULTAT ==> 34 lignes / 1937 Lincoln Berline

    ```SQL
        SELECT productCode,productName, MSRP
        FROM products
        WHERE buyPrice>=60 AND buyPrice<=90
        ORDER BY buyPrice
    ``` 

# COLONNES CALCULEES

> La liste des motos (nom, vendeur, quantité et marge) triés par marge décroissante

    RESULTAT ==> 13 lignes / 2003 Harley-Davidson Eagle Drag Bike

    ```SQL
       SELECT productName, productVendor, quantityInStock, MSRP-buyPrice as marge
        FROM products
        WHERE productLine = "Motorcycles"
        ORDER BY marge DESC 
    ``` 

> La liste des commandes (numéro, date de commande, date d'expédition, écart en jours entre les deux dates et statut) 
qui sont en cours de traitement ou qui ont été expédiées et ont un écart de plus de 10j triés par écart décroissant puis par date de commande
    
    RESULTAT ==> 7 lignes / 10165  - AVEC LE DATEDIFF

    ```SQL
       SELECT orderNumber, orderDate, shippedDate, status, DATEDIFF(shippedDate,orderDate) AS orderdif
        FROM orders
        WHERE status="In Process" OR (status="shipped" AND DATEDIFF(shippedDate,orderDate)>10)  
        ORDER BY orderdif DESC
    ``` 

> La liste des produits (nom et valeur du stock à la vente) des années 1960

    RESULTAT ==> 16 lignes / 1969 Harley Davidson Ultimate Chopper

    ```SQL
       SELECT productName, MSRP*quantityInStock AS total
        FROM products
        WHERE productName LIKE'%196%'
    ``` 

# GROUP BY - Aggrégation

> Le prix moyen d'un produit vendu par chaque vendeur triés par prix moyen décroissant

    RESULTAT ==> 13 lignes / Welly Diecast Productions / 113.9325

    ```SQL
        SELECT productVendor AVG
        FROM products
    	GROUP BY productVendor  
        ORDER BY AVG DESC
    ``` 

> Le nombre de produits pour chaque ligne de produit

    RESULTAT ==> 7 lignes / Classic Cars / 38

    ```SQL
        SELECT COUNT(productLine), productLine
        FROM products
    	GROUP BY productline
    ``` 

> Le total du stock et le total de la valeur du stock à la vente de chaque ligne de produit pour les produits vendus plus de 100$ trié par total de la valeur du stock à la vente

    RESULTAT ==> 7 lignes / Ships / 429177.74

    ```SQL
        SELECT SUM(quantityInStock) AS totalstock, productLine, SUM(quantityInStock*MSRP) AS TOTAL	
        FROM products
        WHERE MSRP>=100
        GROUP BY productLine  
        ORDER BY TOTAL
    ``` 


> La quantité du produit le plus en stock de chaque vendeur trié par vendeur

    RESULTAT ==> 13 lignes / Autoart Studio Design / 9354

     ```SQL
        SELECT MAX(quantityInStock), productVendor
FROM products

GROUP BY productVendor  

	ORDER BY productVendor 
    ``` 

> Le prix de l'avion qui coûte le moins cher à l'achat

    RESULTAT ==> 1 ligne / 29.34$

    ```SQL
    SELECT MIN(buyPrice) AS cheapestPricePlane
    FROM products
    WHERE productLine = 'Planes'
    ```

> Le crédit des clients qui ont payé plus de 20000$ durant l'année 2004 trié par crédit décroissant

    RESULTAT ==> 69 lignes / 141 / 293 765.51

# INNER JOIN

> La liste des employés (nom, prénom et fonction) et des bureaux (adresse et ville) dans lequel ils travaillent

    RESULTAT ==> 23 lignes / Diane Murphy

> La liste des clients français ou américains (nom du client, nom, prénom du contact et pays) et de leur commercial dédié (nom et prénom) triés par nom et prénom du contact

    RESULTAT ==> 48 lignes / Miguel Barajas

> La liste des lignes de commande (numéro de commande, code, nom et ligne de produit) et la remise appliquée aux voitures ou motos commandées triées par numéro de commande puis par remise décroissante

    RESULTAT ==> 2026 lignes / 34

# SELECT COMPLEXES

> Le total des paiements effectués de chaque client (numéro, nom et pays) américain, allemand ou français de plus de 50000$ trié par pays puis par total des paiements décroissant

    RESULTAT ==> 38 lignes / 146 / 130305.35

> Le montant total de chaque commande (numéro et date) des clients New-Yorkais (nom) trié par nom du client puis par date de commande

    RESULTAT ==> 16 lignes / Classic Legends / 10115 / 21665.98