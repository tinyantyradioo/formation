<?php
require_once 'vendor/autoload.php';

/** Configuration */
require_once('config/config.php');

/** Librairie BDD */
require_once('lib/db.php');

/** Librairie APP */
require_once('lib/app.php');

echo '<p> ici on ajoute des users fake </p>';



$width = 100;
$height = 100;

$faker = Faker\Factory::create('fr_FR');

$values = [];
$username = $faker->name();
$firstname = $faker->firstName();
$lastname = $faker->lastName();
$email = $faker->email();
$avatar = $faker->imageUrl($width, $height, 'avatar');
$created_at = $faker->dateTime();
$password = $faker->password();

// afficher l'image

echo "<img src='" . $avatar . "'/>";

for ($i = 0; $i < 1; $i++) {

    $values[] = $faker->name();
    $values[] = $faker->firstName();
    $values[] = $faker->lastName();
    $values[] = $faker->email();
    $values[] = $faker->imageUrl();
    $values[] = $faker->dateTime();
    $values[] = $faker->password();
}
var_dump($values);


class User
{

    private $dbh;


    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->dbh = connect();
    }

    /** Ajoute un salon dans la base de donnée
     * @param string $username
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param string $avatar
     * @param DateTime $created_at
     * @param string $password
     * @return mixed
     */


    public function add(string $username, string $firstname, string $lastname, string $email, string $avatar, DateTime $created_at, string $password):int
    {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('INSERT INTO user (username, firstname, lastname, email, avatar, created_at, password) 
                VALUES (:username, :firstname, :lastname, :email, :avatar, :created_at, :password)');


            //3. Lier les données
            $sth->bindValue('username', $username, PDO::PARAM_STR);
            $sth->bindValue('firstname', $firstname, PDO::PARAM_STR);
            $sth->bindValue('lastname', $lastname, PDO::PARAM_STR);
            $sth->bindValue('email', $email, PDO::PARAM_STR);
            $sth->bindValue('avatar', $avatar, PDO::PARAM_STR);
            $sth->bindValue('created_at', $created_at->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('password', $password, PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();

            // 5. recuperation de l'id user 
            $last_insert_id_user = $this->dbh->lastInsertId();
            var_dump('$last_insert_id');
            echo 'Last Insert ID: ' . $last_insert_id_user;
            return $last_insert_id_user;



        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }


}


// On a besoin d'une instance 
$newUser = new User();


// push le user dans la base
$newUser->add($username, $firstname, $lastname, $email, $avatar, $created_at, $password);
