<?php
require('app/models/orders.php');

/** Pas d'erreur au début ! */
$error = null;

// Si le paramètre contenant l'id de la commande est passé dans l'URL (queryString) et qu'il n'est pas vide
// Alors je peux travailler et afficher le BDC !
if (isset($_GET['id']) && !empty($_GET['id'])) {

    /** 1 : on se connecte au serveur MySQL*/
    $dbh = connect();


    $customer = orderGetCustomerById($_GET['id']);


    /** Si le résultat est vide on définie une erreur qui sera affichée dans la vue */
    if ($customer === false)
        $error = 'Commande non trouvée !';

    if ($error == null) {
        // si j'ai pas d'erreur je continue le JOB ! Commande trouvée sélection produits et total

        /** REQUETE 2 : les lignes de la commande */
        $sth = $dbh->prepare('SELECT od.priceEach, od.quantityOrdered, od.orderLineNumber, (od.quantityOrdered * od.priceEach) as totalLine ,p.productCode, p.productName, p.productDescription
                                FROM orderDetails od
                                INNER JOIN products p ON (od.productCode = p.productCode)
                                WHERE orderNumber = :id
                                ORDER BY od.orderLineNumber ASC');
        $sth->bindValue(':id', $_GET['id'], PDO::PARAM_INT);
        $sth->execute();
        $orderLines = $sth->fetchAll();


        /** REQUETE 3 : le total de la commande */
        $sth = $dbh->prepare('SELECT SUM(od.quantityOrdered * od.priceEach) as totalOrder
                                    FROM orderDetails od
                                    WHERE orderNumber = :id');

        $sth->bindValue(':id', $_GET['id'], PDO::PARAM_INT);
        $sth->execute();
        $totalOrder = $sth->fetch();
    }

    /** Appel à la vue pour générer l'HTML */
    include('app/views/order/detail.phtml');
} else {
    header('Location: index.php');
}