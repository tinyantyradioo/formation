<?php

const TABLE = 'orders';

function orderGetAll(): ?array 
{
    /** 1 : on se connecte au serveur MySQL*/
    $dbh = connect();

    /** 2 : on préparer notre requête ! */
    $sth = $dbh->prepare('SELECT * FROM '. TABLE);

    /** 3 : on exécute la requête SQL ! */
    $sth->execute();

    /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
    return  $sth->fetchAll();

}


function orderGetCustomerById(int $id) : ?array
{
    /** 1 : on se connecte au serveur MySQL*/
    $dbh = connect();

    /** 2 : on préparer notre requête ! */
    $sth = $dbh->prepare('SELECT * 
                                FROM customers c
                                INNER JOIN orders o ON (c.customerNumber = o.customerNumber)
                                WHERE orderNumber = :id');

    /** 2.1 On va lier les jetons avec les valeurs  */
    $sth->bindValue(':id', $id, PDO::PARAM_INT);

    /** 3 : on exécute la requête SQL ! */
    $sth->execute();

    /** 4 : on récupère le jeu d'enregistrement
     * On sait que l'on a une seule ligne : on utilise donc FETCH
     */
    return $sth->fetch();
    
}