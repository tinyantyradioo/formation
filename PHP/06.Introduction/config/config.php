<?php
/** Default values */
const DEFAULT_CONTROLLER = 'home';

const DEBUG = true;

/** CONNEXION BDD */
const DB_DSN = 'mysql:dbname=classicModels;host=localhost;charset=UTF8';
const DB_USER = 'root';
const DB_PASSWORD = '';


/** MONEY FORMAT */
const MONEY_DEVISE = '€';