<?php
// FRONT CONTROLLER / Routage

/** Import de la configuration */
require('config/config.php');

/** Import des libraires */
require('app/lib/db.php');
require('app/lib/number.php');

/** Initialisation des données */
$controller = DEFAULT_CONTROLLER;


// On vérifie si un controller est fourni en entrée !
if(isset($_GET['c']) && ! empty($_GET['c'])) {
    $controller = $_GET['c'];
}

//On route vers le bon controller => on vérife s'il existe ! 
if(file_exists('app/controllers/'. $controller.'.php')) {
    try {
        // Je temporise l'envoi des données au buffer de sortie
        ob_start();

        // On inclue / execute le controller demandé
        require('app/controllers/' . $controller . '.php');

        // Je récupère les données transmises au buffer dans une variable
        $content = ob_get_contents();

        // Je vide le buffer de sortie qui ne sera donc pas transmis
        ob_end_clean();
    }
    catch (PDOException $e) {
        // On attrape et gère les exceptions de PDO
        if(DEBUG === false)
            $content = 'Desolé une erreur de base de données est arrivée... Zorro est parti !';
        else {
            $content = $e->getMessage();
            $content .= print_r($e->getTrace(),true);
        }
    }
    catch (Exception $e) {
        // On attrape et gère les autres exceptions éventuelles
        if (DEBUG === false)
            $content = 'Desolé une exception inconnue a été levée... Vous êtes maintenant perdu !';
        else {
            $content = $e->getMessage();
            $content .= print_r($e->getTrace(), true);
        }
    }
}
else {
    http_response_code(404);
    $content = '<p>Tu es perdu !</p>';
    $content .= '<img src="images/404.jpg">';
}

require('app/views/layout.phtml');