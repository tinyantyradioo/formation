<?php

/** Fonction de connexion à la base de données
 * 
 * @return PDO l'objet PDO représentant la connexion à la bdd
 */
function connect() : PDO 
{
    /** 1 : on se connecte au serveur MySQL*/
    $dbh = new PDO(DB_DSN, DB_USER, DB_PASSWORD);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    return $dbh;
}

function dateTime()
{
    date_default_timezone_set('Europe/Paris');
}
