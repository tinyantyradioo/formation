<?php

function userIsConnected() {
    if (!isset($_SESSION['connected']) || $_SESSION['connected'] != true) {
        header('Location: login.php');
        exit();
    }

    return true;
}

/** function addFlashBag
 * Ajoute une valeur au flashbag
 * @param string $texte le message a afficher
 * @param string $level le niveau du message (correspond au type d'info bulle boostrap : success - warning - danger ...)
 * @return void
 */
function addFlashBag($texte, $level = 'success')
{
    if (!isset($_SESSION['flashbag']) || !is_array($_SESSION['flashbag']))
        $_SESSION['flashbag'] = array();

    $_SESSION['flashbag'][] = ['message' => $texte, 'level' => $level];
}

/** function getFlashBag
 * Ajoute une valeur au flashbag
 * @param void
 * @return array flashbag le tableau contenant tous les messages a afficher
 */
function getFlashBag()
{
    if (isset($_SESSION['flashbag']) && is_array($_SESSION['flashbag'])) {
        $flashbag = $_SESSION['flashbag'];
        unset($_SESSION['flashbag']);
        return $flashbag;
    }
    return false;
}