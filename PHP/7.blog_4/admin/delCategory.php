<?php
session_start();
/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model User */
require('../models/Category.php');

$view = '';
$pageTitle = '';


// Création d'une instance du model Category
$categoryModel = new Category();

if(isset($_POST['id'])) {
    $id = $_POST['id'];
    if (!empty($categoryModel->getById($id))) {
        $categoryModel->delete($id);
        addFlashBag('La catégorie a bien été supprimée !','success');
    }
    else {
        addFlashBag('La catégorie n\'existe pas !','warning');
    }

    header('Location: listCategory.php');
    exit();

}
