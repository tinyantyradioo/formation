<?php
session_start();

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model User */
require('../models/Category.php');

userIsConnected();

$view = 'listCategory';
$pageTitle = 'Liste des catégories';


// Création d'une instance du model Category
$categoryModel = new Category();
$categories = $categoryModel->getAll();

// Récupère les données du flashbag
$flashbag = getFlashBag();

//var_dump($categories);
/** Appel à la vue pour générer l'HTML */
include('views/layout.phtml');
