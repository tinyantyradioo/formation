<?php
session_start();

unset($_SESSION['connected']);
unset($_SESSION['user']);

header('Location: login.php');
exit();