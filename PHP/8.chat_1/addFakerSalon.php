<?php
require_once 'vendor/autoload.php';

/** Configuration */
require('config/config.php');

/** Librairie BDD */
require('lib/db.php');

/** Librairie APP */
require('lib/app.php');

echo '<p> ici on ajoute des salons fake </p>' ;

session_start();

$width = 100;
$height = 100;

// use the factory to create a Faker\Generator instance
$faker = Faker\Factory::create('fr_FR');



// chat\user
// echo $faker->name(). '<br>';
// echo $faker->firstName(). '<br>';
// echo $faker->lastName(). '<br>';
// echo $faker->email() . '<br>';

// $image = $faker->imageUrl($width, $height, 'avatar') ;
// echo "<img src='" . $image . "'/>" . '<br>';

// echo $faker->date('Paris') . '<br>';
// echo $faker->password() . '<br>';


// echo  '<br>'.'<br>'.'<br>'.'<br>';

// // chat\message
// echo $faker->paragraph(). '<br>';
// echo $faker->name(). '<br>';
// echo $faker->date('Paris') . '<br>';


// echo  '<br>'.'<br>'.'<br>'.'<br>';

// // chat\salon
// echo $faker->name(). '<br>';
// echo $faker->paragraph(). '<br>';
// echo $faker->date('Y-m-d H:i:s') .'<br>';


$values = [];
$name = $faker->name();
$description = $faker->paragraph();
$createdAt = $faker->dateTime();
$slug = $faker->slug();
$avatar = $faker->imageUrl($width, $height, 'avatar') ;


for ($i=0; $i < 1; $i++) { 
    
    $values []= $faker->name();
    $values []= $faker->paragraph();
    $values []= $faker->dateTime();
}
var_dump($values);



class Salon {
    
    private $dbh;

    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->dbh = connect();
    }

    public function getAll() {
        try {

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare('SELECT * FROM salon');

            /** 3 : on exécute la requête SQL ! */
            $sth->execute();

            /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
            return $sth->fetchAll();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }

    }

    
    /** Ajoute un salon dans la base de donnée
     * @param string $name
     * @param string $description
     * @param DateTime $createdAt
     * @param string|null $slug
     * @return mixed
     */
    public function add(string $name, string $description, DateTime $createdAt, ?string $slug = null) {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('INSERT INTO salon (name, description, created_at, slug) 
                VALUES (:name, :description, :createdAt, :slug)');


            //3. Lier les données
            $sth->bindValue('name', $name, PDO::PARAM_STR);
            $sth->bindValue('description', $description, PDO::PARAM_STR);
            $sth->bindValue('createdAt', $createdAt->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('slug', $slug, PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }


}


// On a besoin d'une instance du model CATEGORIE
$newSalon= new Salon();

// push le salon dans la base
    $newSalon->add($name, $description, $createdAt, $slug);



