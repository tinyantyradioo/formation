-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 09 nov. 2021 à 17:17
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `chat`
--

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `message` varchar(45) NOT NULL,
  `user` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime DEFAULT NULL,
  `salon` varchar(45) NOT NULL,
  `salon_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_salon_idx` (`salon_id`),
  KEY `fk_message_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `salon`
--

DROP TABLE IF EXISTS `salon`;
CREATE TABLE IF NOT EXISTS `salon` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `slug` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `salon`
--

INSERT INTO `salon` (`id`, `name`, `description`, `created_at`, `slug`) VALUES
(5, 'Maurice de Martel', 'Culpa laudantium molestias officia cumque aliquam qui animi qui. Et architecto quisquam omnis et reiciendis. Neque deleniti praesentium sapiente repudiandae velit minus.', '1986-08-13 02:24:00', 'repellat-qui-nam-ut'),
(6, 'Andrée Blanchard-Ferrand', 'Quidem eum laboriosam odio sed sed. Ea magnam ullam ut saepe. Dolorum est quisquam aperiam dolores sed laudantium. Et numquam at est unde.', '2019-12-24 12:50:00', 'quidem-et-delectus-possimus-aut-sunt-sapiente'),
(7, 'Élise Marques', 'Et aperiam culpa corporis qui. Non earum nihil nam dolore repellendus maxime et. Dicta illo quo nihil assumenda.', '1973-11-14 06:43:00', 'eligendi-sint-repudiandae-nobis-repudiandae-illum'),
(8, 'Maurice Dupuis', 'Dolores voluptas eius a commodi dolor. Vel autem minus voluptatem beatae pariatur. Quas culpa est fugit maiores nisi natus ut.', '1971-11-03 07:21:00', 'dolores-at-rerum-molestiae-repudiandae-nemo-et-et'),
(9, 'François Hubert', 'Ratione quia reiciendis suscipit est nesciunt iste. Excepturi quaerat omnis et porro. Similique alias nulla accusantium harum eius reprehenderit quas velit.', '1983-01-21 19:29:00', 'molestiae-ipsa-nihil-laboriosam-voluptate'),
(10, 'Joseph Le Humbert', 'Voluptatem veniam velit commodi qui. Rerum aut quod nulla culpa exercitationem. Vero suscipit distinctio odio voluptates.', '1988-02-11 11:20:00', 'fugit-non-reiciendis-nihil'),
(11, 'Léon de la Lebon', 'Vero aut est ipsum quia. Tempora sunt accusamus et. Qui ut ut quis saepe quia odio beatae. Consectetur sint ea minus et.', '1987-06-10 13:49:00', 'voluptatem-rem-ut-nostrum');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(150) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `password` varchar(265) NOT NULL,
  `statut` varchar(150) DEFAULT NULL,
  `connected_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `password_UNIQUE` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `username`, `firstname`, `lastname`, `email`, `avatar`, `created_at`, `password`, `statut`, `connected_at`) VALUES
(1, 'Martin Morel', 'Édouard', 'Germain', 'asimon@leblanc.com', 'https://via.placeholder.com/100x100.png/0000bb?text=avatar+adipisci', '2009-12-13 20:44:00', 'fa}s*Cbkl\')\'.fP', NULL, NULL),
(2, 'Céline Fernandez-Torres', 'Luc', 'Mendes', 'fischer.aurelie@hotmail.fr', 'https://via.placeholder.com/100x100.png/00ddee?text=avatar+rerum', '2012-08-03 00:10:00', 'fThuWM>U_A40', NULL, NULL),
(3, 'Paul Weber', 'William', 'Poulain', 'charles.arnaud@noos.fr', 'https://via.placeholder.com/100x100.png/0011ff?text=avatar+libero', '2014-12-24 07:43:00', '?KaMzI~', NULL, NULL),
(4, 'Corinne-Inès Brunel', 'Alexandre', 'Begue', 'torres.jerome@orange.fr', 'https://via.placeholder.com/100x100.png/004488?text=avatar+autem', '2017-08-08 13:36:00', '9|Nk/ZwqyaR)aC@1DM', NULL, NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `fk_message_salon` FOREIGN KEY (`salon_id`) REFERENCES `salon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_message_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
