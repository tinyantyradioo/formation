-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 03, 2021 at 07:47 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `a_id` int(10) UNSIGNED NOT NULL,
  `a_title` varchar(100) NOT NULL,
  `a_content` text,
  `a_created_at` datetime NOT NULL,
  `a_published_at` datetime NOT NULL,
  `a_modified_at` datetime DEFAULT NULL,
  `a_picture` varchar(100) DEFAULT NULL,
  `a_slug` varchar(100) NOT NULL,
  `a_status` tinyint(4) NOT NULL DEFAULT '1',
  `category_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`a_id`),
  UNIQUE KEY `a_slug_UNIQUE` (`a_slug`),
  KEY `fk_article_category_idx` (`category_id`),
  KEY `fk_article_user1_idx` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `c_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `c_title` varchar(100) NOT NULL,
  `c_description` text,
  `c_created_at` datetime NOT NULL,
  `c_picture` varchar(100) DEFAULT NULL,
  `c_slug` varchar(100) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`c_id`, `c_title`, `c_description`, `c_created_at`, `c_picture`, `c_slug`) VALUES
(1, 'Voyage', 'c\'est la meilleur qui parle de voyage.\r\n', '2021-10-20 12:11:08', NULL, ''),
(2, 'azazeaze', 'azeaze', '2021-10-21 07:25:00', '', 'azazeaze'),
(3, 'de type pas interessant', 'ouai ', '2021-10-21 08:05:00', '', 'de type pas interessant'),
(4, 'éééééééééé', 'ééééé', '2021-10-21 08:10:00', '', 'éééééééééé'),
(5, 'azeeee', 'aeaeae', '2021-10-21 14:23:00', '', 'azeeee'),
(6, '&é\"&é\"', '&é\"&é\"', '2021-10-21 14:54:00', '', '&é\"&é\"'),
(7, 'test', 'zerzer', '2021-10-21 14:58:00', '', 'test'),
(9, 'omegatest', '<p>azeazeaze</p>', '2021-10-27 16:18:00', 'omegatest_61795fcc277d3.jpg', 'omegatest'),
(10, 'test______5', '<p>e\'te\'rterertetr</p>', '2021-10-27 16:24:00', 'test______5_6179612808300.jpg', 'test______5'),
(12, '&é\"&é\"', '<p>azeazeaefff</p>', '2021-10-27 16:36:00', '&é\"&é\"_617963d122ba2.png', '&é\"&é\"'),
(14, 'qsd', '<p>dfqssd</p>', '2021-10-27 16:38:00', 'qsd_61796450f085c.jpg', 'qsd');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `c_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `c_created_at` datetime NOT NULL,
  `c_user_name` varchar(45) NOT NULL,
  `c_user_email` varchar(45) NOT NULL,
  `c_comment` text NOT NULL,
  `c_status` tinyint(4) NOT NULL DEFAULT '0',
  `article_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`c_id`),
  KEY `fk_comment_article1_idx` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `u_firstname` varchar(45) NOT NULL,
  `u_lastname` varchar(45) NOT NULL,
  `u_email` varchar(150) NOT NULL,
  `u_password` varchar(255) NOT NULL,
  `u_created_at` datetime NOT NULL,
  `u_connected_at` datetime DEFAULT NULL,
  `u_role` varchar(45) NOT NULL DEFAULT 'ROLE_AUTH',
  `u_status` tinyint(4) NOT NULL DEFAULT '1',
  `u_avatar` varchar(45) DEFAULT NULL,
  `u_bio` text,
  PRIMARY KEY (`u_id`),
  UNIQUE KEY `u_email_UNIQUE` (`u_email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`u_id`, `u_firstname`, `u_lastname`, `u_email`, `u_password`, `u_created_at`, `u_connected_at`, `u_role`, `u_status`, `u_avatar`, `u_bio`) VALUES
(1, 'florent', 'Lou', 'lou_il_est@gentildeouf.fr', '$2y$10$MWFfVmmDduiVWPx/hpVOmeMmMjUdKdbzJoSWRRHsuPsAJa/yD53ya', '2021-10-20 09:48:00', NULL, 'ROLE_AUTH', 1, NULL, '*'),
(2, 'admin', 'admin', 'admin@admin.admin', '$2y$10$5PlowbQx8xW.FY6BfYwaqOoe08TiObFbNuFba9fEilcDfNK8jQyuS', '2021-10-27 12:53:00', NULL, 'ROLE_ADMIN', 1, NULL, '<p>l\'admin mot de passe: adminadmin</p>'),
(3, 'admin', 'admin', 'admin@admin.adminadmin', '$2y$10$7ZmleP.CFzAyWp.MRNtYduccXuqeIz2A/KdfvQ995qn.8SBC7BaES', '2021-11-03 08:54:00', NULL, 'ROLE_ADMIN', 1, NULL, 'LE admin');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;