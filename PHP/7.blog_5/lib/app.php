<?php

function userIsConnected() {
    if (!isset($_SESSION['connected']) || $_SESSION['connected'] != true) {
        header('Location: login.php');
        exit();
    }

    return true;
}

/** function addFlashBag
 * Ajoute une valeur au flashbag
 * @param string $texte le message a afficher
 * @param string $level le niveau du message (correspond au type d'info bulle boostrap : success - warning - danger ...)
 * @return void
 */
function addFlashBag($texte, $level = 'success')
{
    if (!isset($_SESSION['flashbag']) || !is_array($_SESSION['flashbag']))
        $_SESSION['flashbag'] = array();

    $_SESSION['flashbag'][] = ['message' => $texte, 'level' => $level];
}

/** function getFlashBag
 * Ajoute une valeur au flashbag
 * @param void
 * @return array flashbag le tableau contenant tous les messages a afficher
 */
function getFlashBag()
{
    if (isset($_SESSION['flashbag']) && is_array($_SESSION['flashbag'])) {
        $flashbag = $_SESSION['flashbag'];
        unset($_SESSION['flashbag']);
        return $flashbag;
    }
    return false;
}

function dateTime()
{
    date_default_timezone_set('Europe/Paris');
}

/** UPLOAD */

/** Supprime un fichier sur le disque
 * @param string $file chemin absolu ou relatif d'un fichier sur le disque du serveur
 * @return boolean true si suppression ou false sinon
 */
function deleteFile(string $file) : bool
{
    if (file_exists($file) && !is_dir($file)) {
        if (unlink($file))
            return true;
    }
    return false;
}

/** Déplace un fichier transmis dans un répertoire du serveur
 * @param $postName nom de l'index dans le tableau $_FILES
 * @param $folder chemin absolue ou relatif où le fichier sera déplacé
 * @return mixed nom du fichier ou null si l'upload a échoué
 */
function uploadFile(string $fileName, string $folder, array $validFile = UPLOADS_VALIDE_IMG )
{
  
    if (isset($_FILES[$fileName]) && !empty($_FILES[$fileName]["tmp_name"]) && $_FILES[$fileName]["error"] == UPLOAD_ERR_OK) {
        $tmp_name = $_FILES[$fileName]["tmp_name"];
        // basename() peut empêcher les attaques de système de fichiers;
        // la validation/assainissement supplémentaire du nom de fichier peut être approprié
        //on récupère le nom est l'extension du fichier
        $tmpFileInfo = explode('.', basename($_FILES[$fileName]['name']));
        // On met les données dans un tableau associatif et l'extention en minuscule
        $fileInfo['name'] = $tmpFileInfo[0];
        $fileInfo['ext'] = strtolower($tmpFileInfo[count($tmpFileInfo) - 1]);

        /** On test si l'extension et le type MIME sont bon ! */
        if(!array_key_exists($fileInfo['ext'], UPLOADS_VALIDE_IMG) || !in_array($_FILES[$fileName]['type'], UPLOADS_VALIDE_IMG))
            return false;

        // A partir de ça je créé un nom de fichier unique (nom du fichier + uniqid() + extension)
        $name = $fileInfo['name'] . '-' . uniqid() . '.' . $fileInfo['ext'];
        // Je déplace le fichier temporaire dans le bon dossier
        if (move_uploaded_file($tmp_name, UPLOADS_DIR . $folder . '/' . $name))
            return $name;
    }

    return null;

}