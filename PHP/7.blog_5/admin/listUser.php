<?php
session_start();

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Inclure le model User */
require('../models/User.php');

$view = 'listUser';
$pageTitle = 'Liste des utilisateurs';

/**
 * 1. Récupérer la liste des utilisateurs dans la base de données
 *      1.1 Connexion à la base de données
 *      1.2 Préparer notre requête (SELECT)
 *      1.3 Exécuter la requête
 *      1.4 Récupérer les enregistrement obtenus ! (fetchAll)
 * 
 * 2. Générer la sortie HTML à partir de notre Layout et vue !
 */

 // Création d'une instance du model User
 $userModel = new User();
 $users = $userModel->getAll();

//var_dump($users);
/** Appel à la vue pour générer l'HTML */
include('views/layout.phtml');
