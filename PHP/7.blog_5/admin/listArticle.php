<?php
session_start();

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model User */
require('../models/Article.php');



$view = 'listArticle';
$pageTitle = 'Liste des articles';


// Création d'une instance du model Category
$articleModel = new Article();
$articles = $articleModel->getAll();

// Récupère les données du flashbag
$flashbag = getFlashBag();

//var_dump($categories);
/** Appel à la vue pour générer l'HTML */
include('views/layout.phtml');
