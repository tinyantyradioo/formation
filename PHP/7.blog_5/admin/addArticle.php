<?php
session_start();

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Librairie APP */
require('../lib/app.php');

/** Inclure le model Catégorie et Article */
require('../models/Category.php');
require('../models/Article.php');



// Mode édition ou ajout ?
$id = (!empty($_GET['id'])) ? $_GET['id'] : null;


/** Les variables nécessaire pour le Layout */
$view = 'addArticle';
$pageTitle = (empty($id)) ? 'Ajout d\'un article' : 'Modifier article';


/** Les erreurs eventuelles lors de la récupération du formulaire */
$errors = [];

// On a besoin d'une instance du model CATEGORIE
$articleModel = new Article();

if (!empty($id)) {
    $article = $articleModel->getById($id);
    $title = $article['a_title'];
    $content = $article['a_content'];
    $pictureCurrent = $article['a_picture'];
    $slug = $article['a_slug'];
    $publishedAt = $article['a_published_at'];
    $c_id = $article['cartegorie'];
} else {

    /** Initialisation des variables du formulaire pour injecter les données */
    $title = '';
    $content = '';
    $picture = '';
    $slug = '';
    $pictureCurrent = '';
    $publishedAt = '';
    $c_id = '';
}




$allCates = new Category;
// var_dump($allCates->getAll());

/** Si mon formulaire est posté ! */
if (isset($_POST['title'])) {
    // On vérifie si l'ID est transmis / mode édition
    $id = (!empty($_POST['id'])) ? $_POST['id'] : null;

    // On remplie les variables pour récupérer le contenu des champs et les réinjecter si nécessaire dans le formulaire
    $title = $_POST['title'];
    $content = $_POST['content'];
    $picture = null;
    $c_id = (int)$_POST['categorie'];
    



    /** On teste s'il y a des erreurs sur les champs du formulaire  */

    // validation du champ title
    if (empty($title) || strlen($title) < 3) {
        $errors['title'] = true;
    }


    if (!empty($articleModel->getBySlug($slug, $id))) {
        $errors['slug2'] = true;
    }

    // UPLOADER L'IMAGE 
    if (isset($_FILES['picture']) && $_FILES['picture']['error'] == UPLOAD_ERR_OK) {
        // Je sépare le nom du fichier de son extension
        $file = explode('.', basename($_FILES['picture']['name']));
        // A partir de ça je créé un nom de fichier unique
        $picture = $file[0] . '-' . uniqid() . '.' . $file[count($file) - 1];
        // Je déplace le fichier temporaire dans le bon dossier
        move_uploaded_file($_FILES['picture']['tmp_name'], '../uploads/article/' . $picture);
        //Si on a une valeur dans $pictureCurrent et que le fichier existe on le supprime sur le disque
        if ($pictureCurrent != null && file_exists('../uploads/article/' . $pictureCurrent))
            unlink('../uploads/article/' . $pictureCurrent);
    } else {
        $picture = $pictureCurrent;
    }


    // Enfin s'il n'y a pas d'erreur on enregistre dans la base :
    if (empty($errors)) {

        $createdAt = new DateTime('now', new DateTimeZone('Europe/Paris'));

        if (empty($id)) {
            //Ajout du message flash
            addFlashBag('La catégorie a bien été ajoutée !', 'success');
            // On ajoute la catégorie
            $articleModel->add($title, $createdAt, $content, $picture, (int)$c_id);
        } else {
            //Ajout du message flash
            addFlashBag('La catégorie a bien été modifée !', 'success');
            // mon modifie la catégorie !
            $articleModel->update($id, $title, $slug, $content, $picture);
        }



        // On redirige vers la liste des catégories
        header('Location: listArticle.php');
        exit();
    }
}

//var_dump($errors);
require('views/layout.phtml');

