<?php



/**
 * Category
 */
class Category {
    
    private $dbh;

    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->dbh = connect();
    }

    /** Retourne toutes les catégories
     * @return array|false le jeu d'enregistrement ou false si une erreur survient
     */
    public function getAll()
    {
        try {

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare('SELECT * FROM category');

            /** 3 : on exécute la requête SQL ! */
            $sth->execute();

            /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
            return $sth->fetchAll();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }

    }

    /** Retourne toutes les catégories
     * @param int $id identfiant de la catégorie
     * @return array|false le jeu d'enregistrement ou false si une erreur survient
     */
    public function getById(int $id)
    {
        try {

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare('SELECT * FROM category WHERE c_id = :id');

            /** 3 : on exécute la requête SQL ! */
            $sth->bindValue('id',$id, PDO::PARAM_INT);
            $sth->execute();

            /** 4 : on récupère la ligne du jeu d'enregistrement */
            return $sth->fetch();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }
    }

    /** Retourne une catégorie par rapport à son slug
     * @param string $slug le slug recherché dans la table category
     */
    public function getBySlug(string $slug, int $excludeId=null) {
        try {

            $sth = $this->dbh->prepare('SELECT c_slug 
            FROM category 
            WHERE c_slug = :slug AND c_id != :id');

            $sth->bindValue('slug', $slug, PDO::PARAM_STR);
            $sth->bindValue('id', $excludeId, PDO::PARAM_INT);

            $sth->execute();

            $values = $sth->fetch();

            return $values;

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }



    /**
     * add
     *
     * @param  string $title
     * @param  DateTime $createdAt
     * @param  string $slug
     * @param  string $picture
     * @param  string $description
     * @return void
     */
    public function add(string $title, DateTime $createdAt, string $slug, ?string $description =null, ?string $picture = null) {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('INSERT INTO category (c_title, c_description, c_created_at, c_picture, c_slug) 
                VALUES (:title,:description, :createdAt, :picture, :slug)');


            //3. Lier les données
            $sth->bindValue('title', $title, PDO::PARAM_STR);
            $sth->bindValue('description', $description, PDO::PARAM_STR);
            $sth->bindValue('createdAt', $createdAt->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('picture', $picture, PDO::PARAM_STR);
            $sth->bindValue('slug', $slug, PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    /**
     * modification
     ** @param  int $id
     * @param  string $title
     * @param  string $slug
     * @param  string $picture
     * @param  string $description
     * @return void
     */
    public function update(int $id, string $title, string $slug, ?string $description = null, ?string $picture = null)
    {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('UPDATE category 
            SET c_title=:title, c_description=:description, c_picture=:picture, c_slug=:slug
            WHERE c_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, PDO::PARAM_INT);
            $sth->bindValue('title', $title, PDO::PARAM_STR);
            $sth->bindValue('description', $description, PDO::PARAM_STR);
            $sth->bindValue('picture', $picture, PDO::PARAM_STR);
            $sth->bindValue('slug', $slug, PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    /** Supprime une catégory
     * @param int $id identifiant de la catégorie
     */
    public function delete(int $id) {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('DELETE FROM category 
            WHERE c_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, PDO::PARAM_INT);

            //4. Executer ma requête
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }
}