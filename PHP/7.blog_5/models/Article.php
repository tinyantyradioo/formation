<?php



/**
 * Article
 */
class Article
{

    private $dbh;


    /**
     * __construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->dbh = connect();
    }

    /** Retourne toutes les articles
     * @return array|false le jeu d'enregistrement ou false si une erreur survient
     */
    public function getAll(): array
    {
        try {

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare('SELECT * FROM article');

            /** 3 : on exécute la requête SQL ! */
            $sth->execute();

            /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
            return $sth->fetchAll();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }
    }

    /** Retourne toutes les articles
     * @param int $id identfiant de la article
     * @return array|false le jeu d'enregistrement ou false si une erreur survient
     */
    public function getById(int $id)
    {
        try {

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare('SELECT * FROM article WHERE a_id = :id');

            /** 3 : on exécute la requête SQL ! */
            $sth->bindValue('id', $id, PDO::PARAM_INT);
            $sth->execute();

            /** 4 : on récupère la ligne du jeu d'enregistrement */
            return $sth->fetch();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }
    }

    /** Retourne une article par rapport à son slug
     * @param string $slug le slug recherché dans la table article
     */
    public function getBySlug(string $slug, int $excludeId = null)
    {
        try {

            $sth = $this->dbh->prepare('SELECT a_slug 
            FROM article 
            WHERE a_slug = :slug AND a_id != :id');

            $sth->bindValue('slug', $slug, PDO::PARAM_STR);
            $sth->bindValue('id', $excludeId, PDO::PARAM_INT);

            $sth->execute();

            $values = $sth->fetch();

            return $values;
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }



    /**
     * add
     *
     * @param  string $title
     * @param  DateTime $createdAt
     * @param  string $picture
     * @param  string $content
     * @return void
     */
    public function add(string $title, DateTime $createdAt, ?string $content = null, ?string $picture = null, int $c_id)
    {
        // $cate = new Category;
        // $cate = $cate->getById((int)$c_id);
        // var_dump($c_id);
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('INSERT INTO article (a_title, a_content, a_created_at, a_picture, a_slug, category_id) 
                VALUES (:title,:content, :createdAt, :picture, :slug, :c_id)');


            //3. Lier les données
            $sth->bindValue('title', $title, PDO::PARAM_STR);
            $sth->bindValue('content', $content, PDO::PARAM_STR);
            $sth->bindValue('createdAt', $createdAt->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('slug', $this->slugify($title), PDO::PARAM_STR);
            $sth->bindValue('picture', $picture, PDO::PARAM_STR);
            $sth->bindValue('c_id', $c_id, PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    /**
     * modification
     ** @param  int $id
     * @param  string $title
     * @param  string $slug
     * @param  string $picture
     * @param  string $content
     * @return void
     */
    public function update(int $id, string $title, string $slug, ?string $content = null, ?string $picture = null)
    {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('UPDATE article 
            SET a_title=:title, a_content=:content, a_picture=:picture, a_slug=:slug
            WHERE a_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, PDO::PARAM_INT);
            $sth->bindValue('title', $title, PDO::PARAM_STR);
            $sth->bindValue('content', $content, PDO::PARAM_STR);
            $sth->bindValue('picture', $picture, PDO::PARAM_STR);
            $sth->bindValue('slug', $slug, PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    /** Supprime un article
     * @param int $id identifiant de la article
     */
    public function delete(int $id)
    {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('DELETE FROM article 
            WHERE a_id=:id');

            //3. Lier les données
            $sth->bindValue('id', $id, PDO::PARAM_INT);

            //4. Executer ma requête
            $sth->execute();
        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }

    public function slugify($text, string $divider = '-')
    {
        // replace non letter or digits by divider
        $text = preg_replace('~[^\pL\d]+~u', $divider, $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider
        $text = preg_replace('~-+~', $divider, $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }
}
