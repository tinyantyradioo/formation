<?php

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');



/** Les variables nécessaire pour le Layout */
$view = 'addArticle';
$pageTitle = 'Ajout d\'un article';


require('views/layout.phtml');