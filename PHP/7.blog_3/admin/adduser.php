<?php

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Inclure le model User */
require('../models/User.php');

/** Les variables nécessaire pour le Layout */
$view = 'addUser';
$pageTitle = 'Ajout d\'un utilisateur';


/** Les erreurs eventuelles lors de la récupération du formulaire */
$errors = [];

/** Initialisation des variables du formulaire pour injecter les données */
$firstname = '';
$lastname = '';
$email = '';
$role = '';
$bio = '';
$valid = false;

//var_dump($_POST);

/** Si mon formulaire est posté ! */
if(isset($_POST['email'])) {

    // On a besoin d'une instance du model USER
    $userModel = new User();

    // On remplie les variables pour récupérer le contenu des champs et les réinjecter si nécessaire dans le formulaire
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $passwordConf = $_POST['passwordConf'];
    $role = $_POST['role'];
    $bio = $_POST['bio'];
    $valid = (isset($_POST['valid']))?true:false;

    /** On teste s'il y a des erreurs sur les champs du formulaire  */

    // Validation du Champ email
    if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
        $errors['email'] = true;
    }

    // L'email est-il déjà dans la base ??
    // On ne peut pas avoir 2 emails identique !
    if(!empty($userModel->getByEmail($email))) {
        $errors['emailDuplicate'] = true;
    }

    // validation du champ firstname
    if(empty($firstname) || strlen($firstname) < 3) {
        $errors['firstname'] = true;
    }

    // validation du champ lastname
    if (empty($lastname) || strlen($lastname) < 3) {
        $errors['lastname'] = true;
    }

    // validation du Champ password
    if(empty($password) || strlen($password) < 8 || $password != $passwordConf) {
        $errors['password'] = true;
    }

    // validation du Champ ROLES 
    if(empty($role) || !array_key_exists($role, ROLES)) {
        $errors['role'] = true;
    }

    // Enfin s'il n'y a pas d'erreur on enregistre dans la base :
    if(empty($errors)) {

        // On hash le mot de passe 
        $password = password_hash($password, PASSWORD_DEFAULT);

        // On ajoute l'utilisateur
        $userModel->add($lastname, $firstname, $email, $password, new DateTime(), $valid, null, $bio, $role);

        // On redirige vers la liste
        header('Location: listUser.php');
        exit();
    }

}

//var_dump($errors);
require('views/layout.phtml');