<?php

/** Configuration */
require('../config/config.php');

/** Librairie BDD */
require('../lib/db.php');

/** Inclure le model cztegorie */
require('../models/category.php');

/** Les variables nécessaire pour le Layout */
$view = 'addCategory';
$pageTitle = 'Ajout d\'une categorie';


/** Les erreurs eventuelles lors de la récupération du formulaire */
$errors = [];
$debug = false;
$validForm = true;

/** Initialisation des variables du formulaire pour injecter les données */
$title = '';
$description = '';
$createAt="";
$picture = '';
$slug = '';
$uploaddir = '';


var_dump($_POST,$debug);
var_dump($_FILES,$debug);

/** Si mon formulaire est posté ! */
if(isset($_POST['title'])) {

    // On a besoin d'une instance du model CAT
    $catModel = new cat();

    // On remplie les variables pour récupérer le contenu des champs et les réinjecter si nécessaire dans le formulaire
    $title = $_POST['title'];
    $description = $_POST['description'];
    // $slug=slugify($title); recuperer la fonction de fab pour slugifier
    $slug = $_POST['title'];

    /** On teste s'il y a des erreurs sur les champs du formulaire  */

    if ($title== ""){
        $erreurs["title"]=true;
    }

    if ($description==""){
        $erreurs["description"]=true;
    }

    

    // validation du champ title
    if(empty($title) || strlen($title) < 3) {
        $errors['title'] = true;
    }

    // on verifie si le slug n'est pas vide
    if (empty($slug)) {
        $error['slug'] = true;
    }

    // on verifie si la description n'est pas vide
    if (empty($description)) {
        $error['description'] = true;
    }

    // on déplace l'image 
    
    // permet de mettre l'image sur le disque dur
    // $mainPic = '';
    // $info = new SplFileInfo($mainPic);
    // $laPic = '../uploads/category/'.$info ;
    //  $mainPic = move_uploaded_file($_FILES["picture"]["tmp_name"], $laPic);
    // var_dump($laPic);

    $info = new SplFileInfo($_FILES['picture']['name']);
    $ext = $info->getExtension();//permet de récupérer l'extension du fichier fournit
    var_dump($ext,$debug);


    // on envoi l'image sur le disque dure
    // $uploadfileInPc = move_uploaded_file($_FILES["picture"]["tmp_name"], $uploaddir);
   
    // verifier si l'extension du fichier est correct (a faire)



    

    // Enfin s'il n'y a pas d'erreur on enregistre dans la base :
    if(empty($errors)) {
        //upload du fichier image:
        $uploaddir = '../upload/category/';//endroit du serveur où télécharger le fichier image
        $uploadfile = $uploaddir.$slug.'.'.$ext;//concaténation de l'endroit ou enregistrer le fichier sur le serveur et du nom du fichier transformé grâce au slug, avec l'extension du fichier fournit

        $info2 = new SplFileInfo($uploadfile);
        $picture = $info2->getBasename('.'.$ext);//récupération du nom du fichier sans l'extension
        
        $picture = $picture.'_'.uniqid().'.'.$ext;//ajout d'un identifiant unique et ajout rajout de l'extension
        //var_dump($picture);
        var_dump($picture,$debug);
        //uniqid

        $createAt = dateTime();//va chercher l'heure sur le fuseau horaire de paris

        // On ajoute la categorie
        $catModel->add($title, $description, new DateTime(), $picture, $slug);

        // On redirige vers la liste
        header('Location: listCategory.php');
        exit();
    }


}


require('views/layout.phtml');