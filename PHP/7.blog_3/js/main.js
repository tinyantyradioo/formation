'use strict';

window.addEventListener("DOMContentLoaded", (event) => {
    console.log("DOM entièrement chargé et analysé");

  

    $(document).ready(function(){
        $("#myModal").modal('show');
    });

    // $(window).load(function(){        
    //     $('#myModal').modal('show');
    //      }); 



            //DataTable
            $(document).ready(function() {
                $('#tableDonnee').DataTable({
                    colReorder: true,
                    stateSave: true,
                    dom: 'Bfrtip',
                    buttons: [
                        'pageLength',
                        'print',
                        'copyHtml5',
                        'csvHtml5',
                        'pdfHtml5',
                        'colvis'
                    ],
                    columnDefs: [{
                        "orderable": false,
                        "targets": -1
                    }],
                    language: {
                        url: 'js/dataTable/i18n/fr.json'
                    }
                });
            });
    
            //TinyMCE
            tinymce.init({
                selector: 'textarea',
                height: 300,
                relative_urls: false,
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste imagetools wordcount"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
                content_css: [
                    'css/bootstrap.min.css',
                    'css/editor.css'
                ],
                images_upload_url: ''
            });
    
    
            //Modal delete Bootstrap
            $('#delete').on('show.bs.modal', function(e) {
                $('#linkdelete').attr("href", e.relatedTarget.href);
            });
            
            $('#close').click(function(){
                $(this).closest('#Model').hide()
            });
  });