<?php

/**
 * User
 */
class Cat {
    
    private $dbh;

    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->dbh = connect();
    }

    public function getAll() {
        try {

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare('SELECT * FROM category');

            /** 3 : on exécute la requête SQL ! */
            $sth->execute();

            /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
            return $sth->fetchAll();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }

    }

    /** Ajoute un utilisateur dans la base de donnée
     * @param string $title
     * @param string $description
     * @param DateTime $createdAt
     * @param string|null $picture
     * @param string|null $slug
     * 
     * @return mixed
     */
    public function add(string $title, string $description, DateTime $createdAt, ?string $picture = null, string $slug = null) {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('INSERT INTO category (c_title , c_description, c_created_at, c_picture, c_slug) 
                VALUES (:title, :description, :createdAt, :picture, :slug)');


            //3. Lier les données
            $sth->bindValue('title', $title, PDO::PARAM_STR);
            $sth->bindValue('description', $description, PDO::PARAM_STR);
            $sth->bindValue('createdAt', $createdAt->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('picture', $picture, PDO::PARAM_STR);
            $sth->bindValue('slug', $slug, PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }


}

