<?php



/**
 * User
 */
class User {
    
    private $dbh;

    
    /**
     * __construct
     *
     * @return void
     */
    public function __construct() {
        $this->dbh = connect();
    }

    public function getAll() {
        try {

            /** 2 : on préparer notre requête ! */
            $sth = $this->dbh->prepare('SELECT * FROM user');

            /** 3 : on exécute la requête SQL ! */
            $sth->execute();

            /** 4 : on récupère le jeu d'enregistrement (tableau PHP) ! */
            return $sth->fetchAll();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
        }

    }

    /** Retourne un utilisateur correspondant à l'email passé en param
     * @param string $email l'email recherché dans la table user
     */
    public function getByEmail(string $email) {
        try {

            $sth = $this->dbh->prepare('SELECT u_email FROM user WHERE u_email = :email');

            $sth->bindValue('email', $email, PDO::PARAM_STR);

            $sth->execute();

            $values = $sth->fetch();

            return $values;

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }


    /** Ajoute un utilisateur dans la base de donnée
     * @param string $lastname
     * @param string $firstname
     * @param string $email
     * @param string $password
     * @param DateTime $createdAt
     * @param bool $status
     * @param string|null $avatar
     * @param string $bio
     * @param string $role
     * 
     * @return mixed
     */
    public function add(string $lastname, string $firstname, string $email, string $password, DateTime $createdAt, bool $status = true, ?string $avatar = null, string $bio = '', string $role = 'ROLE_AUTH') {
        try {

            //2. Préparation de la requête
            $sth = $this->dbh->prepare('INSERT INTO user (u_firstname, u_lastname, u_email, u_password, u_created_at, u_role, u_status, u_avatar, u_bio) 
                VALUES (:firstname,:lastname, :email, :password, :createdAt, :role, :status, :avatar, :bio )');


            //3. Lier les données
            $sth->bindValue('firstname', $firstname, PDO::PARAM_STR);
            $sth->bindValue('lastname', $lastname, PDO::PARAM_STR);
            $sth->bindValue('email', $email, PDO::PARAM_STR);
            $sth->bindValue('password', $password, PDO::PARAM_STR);
            $sth->bindValue('createdAt', $createdAt->format('Y-m-d H:i'), PDO::PARAM_STR);
            $sth->bindValue('role', $role, PDO::PARAM_STR);
            $sth->bindValue('status', $status, PDO::PARAM_INT);
            $sth->bindValue('avatar', $avatar, PDO::PARAM_STR);
            $sth->bindValue('bio', $bio, PDO::PARAM_STR);

            //4. Executer ma requête
            $sth->execute();

        } catch (PDOException $e) {
            echo $e->getMessage();
            var_dump($e->getTrace());
            exit();
        }
    }
}