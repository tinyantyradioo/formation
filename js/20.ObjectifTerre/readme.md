# Objectif terre

Il est temps pour Tintin et Milou de retrouver la terre.

Vous êtes aux commandes. 

En cliquant sur le bouton start le compte à rebours est lancé : 10...9...8...7......0

La fusée décolle !

En analysant les données dans l'HTML et le CSS, vous devriez faire décoller la fusée sans trop de problème.

## A savoir

    Comment fait-on pour faire un chronomètre en Javascript ? 
        Petit indice, c'est un évènement qui va se déclencher toutes les secondes !
        
    La fusée doit décoller au bout de 10 secondes !
    
    Toutes les propriétés des objets du DOM sont accessibles en lectures et en ecriture. Les propriétés peuvent correspondre au propriétés de la balise HTML, par exemple "src" pour la source de l'image (monImage.src = 'toto.jpg') !


## Bonus

Quel beau décollage !!! Et maintenant ?
    A la façon SpaceX, pourquoi ne pas proposer l'arriver sur la terre de notre belle fusée ?

Et si le voyage était un peu plus long ?
    Pourquoi ne pas montrer le parcours de la terre à la lune sur fond noir avec des étoiles plus ou moins grande en quantité aléatoire.
    Vous avez tout ce qu'il vous faut dans le CSS pour créer des étoiles. 
    A vous de les placer correctement dans le DOM.
    Vous pouvez déjà vous entrainer à placer des étoiles lors du décollage.
    Il faudra bien sûr décomposer l'animation !