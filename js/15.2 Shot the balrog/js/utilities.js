'use strict';   // Mode strict du JavaScript

/*************************************************************************************************/
/* *********************************** FONCTIONS UTILITAIRES *********************************** */
/*************************************************************************************************/

/** On renvoie un entier aléatoire entre une valeur min (incluse)
 * et une valeur max (incluse).
 * 
 * @param {Number} min entier minimum inclus
 * @param {Number} max entier maximum inclus
 * 
 */
function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}