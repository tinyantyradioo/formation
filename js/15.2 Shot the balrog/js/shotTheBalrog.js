'use strict';   // Mode strict du JavaScript

// let level = {
//     easy: 1,
//     medium: 2,
//     hard: 3,
// }
let leUser;
let turn = 1;

//declaration des hp max
let maxHpBalrog;
let maxHpGandalf;

// decaration des objets player1 et player2
let player1 = {
    name: 'Gandalf',
    hp: null,
    img1: 'gandalf.png',
    imgGandalfWounded: 'gandalf-wounded.png',
    randomPlay: null,
    randomDamage: null,
    maxHp: null,
}

let player2 = {
    name: 'Balrog',
    hp: null,
    img1: 'balrog.png',
    imgBalrogWoonded: 'gandalf-wounded.png',
    randomPlay: null,
    randomDamage: null,
    maxHp: null,
}
// FIN de decaration des objets player1 et player2

// DEBUT declaration du mode EZ, MEDIUM, HARD
function easyMode() {
    {
        player1.hp = 100 + throwDices(10, 10);
        player2.hp = 100 + throwDices(5, 10);
    }
}

function mediumMode() {
    {
        player1.hp = 100 + throwDices(10, 10);
        player2.hp = 100 + throwDices(10, 10);
    }
}

function hardMode() {
    {
        player1.hp = 100 + throwDices(7, 10);
        player2.hp = 100 + throwDices(10, 10);
    }
}
// FIN declaration du mode EZ, MEDIUM, HARD

// function pow afficher l'image pow des que un joueur atteint 30% de ses pv
function pow() {
    
 
    let hpBalrog30percent = (player2.maxHp/100)*30;
    let hpGandalf30percent = (player1.maxHp/100)*30;

    

    if (player1.hp < hpGandalf30percent) 
        player1.img1 = `gandalf-wounded.png`;
        
    else (player2.hp < hpBalrog30percent)
        player2.img1 = `balrog-wounded.png`;
}
// affichage des pvs a chaque tour
function getBoucled() {
  
    

    document.write('<div class="game-state"> <figure class="game-state_player"> <img src="images/'+ player1.img1 +'" alt="Gandalf"> <figcaption>' + player1.hp + 'PV</figcaption> </figure> <figure class="game-state_player"> <img src="images/'+ player2.img1 +'" alt="Balrog"> <figcaption>' + player2.hp + 'PV</figcaption> </figure></div>');
}
// quand Gandalf gagne = affichage du tour  + image + text avec les degats
function stateOfGame() {
    document.write('<h3>Tour n°' + turn++ + '</h3>');
    document.write('<figure class="game-round"><img src="images/gandalf-Copie.png" alt="Gandalf vainqueur"><figcaption>Vous êtes le plus rapide, vous attaquez le BALROG et lui infligez ' + player1.randomDamage + ' points de dommage !</figcaption></figure>');

    player2.hp = player2.hp -= player1.randomDamage
    
}
// quand Balrog gagne = affichage du tour  + image + text avec les degats
function stateOfGame2() {
    document.write('<h3>Tour n°' + turn++ + '</h3><figure class="game-round"><img src="images/balrog-Copie.png" alt="Balrog vainqueur"><figcaption>Le balrog prend l\'initiative, vous attaque et vous inflige ' + player2.randomDamage + ' points de dommage !</figcaption></figure>')

    player1.hp = player1.hp -= player2.randomDamage
    
}

function whoStart() {
    player1.randomPlay = throwDices(10,6);
    player2.randomPlay = throwDices(10,6);

    if (player1.randomPlay > player2.randomPlay) {
        stateOfGame();
    }
    else stateOfGame2();
}

// pour obtenir throwDices(10, 6);
function throwDices(howManyNbr, typeOfDice) {
    let value = 0;

    for (let i = 1; i <= howManyNbr; i++)
        value += getRandomIntInclusive(1, typeOfDice);

    return value;
}





// attack

function attack() {


    if ( leUser == 'Easy'){

        let dmgMinorEasy = (player1.randomDamage * throwDices(2,6))/100;
        let dmgMajorEasy = (player2.randomDamage * throwDices(2,6))/100;
        
        player1.randomDamage = (throwDices(3,6)) + Math.round(dmgMinorEasy); // majorés de 2D6%.
        player2.randomDamage = (throwDices(3,6)) - Math.round(dmgMajorEasy); // minorés de 2D6%.
    }

    else if ( leUser == 'Hard'){

        let dmgMinorHard = (player1.randomDamage * throwDices(1,6))/100;
        let dmgMajorHard = (player2.randomDamage * throwDices(2,6))/100;

        player1.randomDamage = throwDices(3,6) - Math.round(dmgMinorHard) ; // minorés de 1D6%.
        player2.randomDamage = throwDices(3,6) + Math.round(dmgMajorHard); // majorer de 1d6%.
    } Math.round
   
    if ( leUser == 'Medium'){

    player1.randomDamage = throwDices(3,6); // normal mode
    player2.randomDamage = throwDices(3,6); // normal mode
    }
}  
// application du mode, easy, normal ou hard


    
function difficulty() {
    
    if (leUser == 'Easy')
    easyMode();

    else if (leUser == 'Medium')
    mediumMode();

    else if (leUser == 'Hard')
    hardMode();

}

function endgame() {

    let result = Math.abs(player1.hp);
    let resultBis = Math.abs(player2.hp);

    if (player2.hp >= 0) {
        
        document.write(' <footer><h3>Fin de la partie</h3><figure class="game-end"><figcaption>Vous avez perdu le combat, le dragon vous a carbonisé avec un overkill de '+ result  + '!</figcaption><img src="images/balrog.png" alt="Balrog vainqueur"></figure></footer>');
    }

    else {

        document.write(' <footer><h3>Fin de la partie</h3><figure class="game-end"><figcaption>Vous avez GAGNER le combat, Gandalf est trop STRONG avec un overkill de '+ resultBis + '!</figcaption><img src="images/gandalf.png" alt="Balrog vainqueur"></figure></footer>');
    }
}

/**
 * ------------------------ debut du programme ------------------------************
 **************demande de l'utilisateur***************/
leUser = window.prompt('choisi ta difficulté: Easy, Medium, Hard');
difficulty();
player1.maxHp = player1.hp;
console.log('les hp de base de gandalf sont ' + maxHpGandalf);
player2.maxHp = player2.hp;
console.log('les hp de base de balrog sont ' + maxHpBalrog);
/**
 * ------------------------ debut du programme ------------------------************
 */
while (player1.hp >= 0 && player2.hp >= 0) {
    

    attack();
    whoStart();
    pow();    
    getBoucled();
    console.log(player1.hp, player2.hp); // etat du jeu
    
}

endgame();