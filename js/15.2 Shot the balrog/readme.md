Shot the Balrog
=================

*Shot the Balrog* est un jeu de rôle de combat au tour par tour qui voit s'affronter Gandalf le Gris (le joueur) et un balrog (l'ordinateur).

Le jeu utilise des lancés de dés. Sachez qu'il existe des dés à 6 faces mais également des dés à 7,8, 10 faces ! Nous utiliserons plusieurs types de dés dans ce jeu !

Pour représenter un lancer de dés, nous noterons `nDx`, où **n** est le nombre de dés à lancer, **x** le nombre de faces de chacun des dés. Par exemple 2D6 représente un lancer de 2 dés à 6 faces.

Maquette
--------
La maquette graphique du jeu a été réalisée par un graphiste et un intégrateur à déjà développé la partie HTML/CSS. Vous pouvez donc reprendre directement son travail.
A vous de remplacer la partie dynamique (le déroulement de la partie) pour la générer en javascript.

_Remarque :_ vous disposez pour le moment d'un seul outil pour écrire du code HTML en javascript : **document.write()**. Quelles conséquences pour l'emplacement du **script** dans la page ? Fera-t-on toujours de cette manière ? Votre formateur est là pour répondre avec vous à ces questions. 

Préparation du jeu
------------------

Au lancement du jeu le joueur doit choisir :
	
	- le niveau de difficulté du jeu : facile, normal, difficile

Puis les points de vie de chaque personnage sont tirés au hasard:

	En mode facile :
	--------------
	* PV balrog : 100 + 5D10
	* PV gandalf : 100 + 10D10

	En mode normal :
	--------------
	* PV balrog : 100 + 10D10
	* PV gandalf : 100 + 10D10

	En mode difficile :
	-----------------
	* PV balrog : 100 + 10D10
	* PV gandalf : 100 + 7D10
	
	--> Affichage des PV de départ

Déroulement de la partie
------------------------

A chaque tour de jeu, les étapes suivantes ont lieu :

1) On détermine qui est le plus rapide et attaque l'autre : c'est l'initiative

	Calcul de l'initiative : chaque personnage lance 10D6. Celui qui a le plus grand score attaque l'autre.
	
2) On détermine le nombre de points de dommage causés par l'attaquant à son adversaire

	a) Si c'est le balrog qui attaque :

		Les points de dommages sont égaux à 3D6. 
		Ensuite ces points de dommages sont majorés ou minorés en fonction de la difficulté et de la classe du héro.

		* Niveau facile : les points de dommages sont minorés de 2D6%.
		* Niveau difficile : les points de dommages sont majorés de 1D6%.

	b) Si c'est gandalf qui attaque

		Les points de dommages sont égaux à 3D6. 
		Ensuite ces points de dommages sont majorés ou minorés en fonction de la difficulté et de la classe du héro.

		* Niveau facile : les points de dommages sont majorés de 2D6%.
		* Niveau difficile : les points de dommages sont minorés de 1D6%.
		
3) Affichage du journal du jeu : que s'est-il passé pendant le tour ?

    - Affichage du numéro du tour
	- Affichage de qui a attaqué et combien de points de dommages ont été causés
	- Affichage de l'état du jeu

Fin de la partie
----------------

La partie s'arrête lorsque l'un des personnages est mort, c'est-à-dire qu'il n'a plus de points de vie.

Affichage de l'image du vainqueur

Conseils
--------
Prenez le temps de penser aux différentes étapes à suivre, et essayer de découper la tâche globale en plus petites tâches.
Il faut avancer pas à pas et tester chaque étape. On peut également faire une première version qui ne prend pas en compte tous les paramètres.
Une fois que cette première version fonctionne correctement, on l'enrichit au fur-et-à-mesure avec toutes les fonctionnalités.

Il est plus intéressant - et rassurant - d'avoir rapidement une version simplifée qui fonctionne que l'on enrichit par la suite !

Lorsque le nombre de lignes de code commencent à devenir important, pensez à découper le code en fonctions de manière logique ! 
Les fonctions permettent de ne pas répéter de code mais également de structurer le code pour le rendre plus lisible et plus maintenable (regrouper des instructions par fonctionnalités).

CONSEIL
=======

Conseil 1 : utilisation des objets
---------------------------------------------------- 
Il est plus facile et lisible d'utiliser un objet pour regrouper les valeurs qui sont liées.

Exemple :
```js

//Premère façon de faire 
const game = {};
game.level = 1;
game.pvGandalf = 100;
game.pvDraco = 100;
game.turn = 1;

//Autre façon de faire (identique !)
const game = {
	level : 1,
	pvGandalf : 100,
	pvDraco : 100,
	turn : 1
};

```

Conseil 2 : utilisation des constantes
----------------------------------------------------
On va demander le niveau de jeu à l'utilisateur.
Il pourra choisir 1 pour facile, 2 pour moyen et 3 pour difficile.

Dans notre code nous pourrions donc tester ceci : 
```js
switch(level) { 
	case 1:
		// On fait ce qu'il faut pour le niveau facile
		break;
	case 2:
		// On fait ce qu'il faut pour le niveau moyen
		break;
	//etc
}
```

Et si nous mettions ses valeurs dans des constantes.

Notre code serait beaucoup plus lisible et compréhensible.

```js
const LEVEL_EASY = 1;
const LEVEL_MEDIUM = 2;

// plein de code ici


switch(level) { 
	case LEVEL_EASY:
		// On fait ce qu'il faut pour le niveau facile
		break;
	case LEVEL_MEDIUM:
		// On fait ce qu'il faut pour le niveau moyen
		break;
	//etc
}
```

**Just do it !**

	
BONUS
=====

Bonus n°1 : images de Gandalf et du Balrog amochés
----------------------------------------------------
Lors de l'affichage de l'état du jeu, si les points de vie des personnages sont inférieurs à 30% de leurs points de vie de départ, 
l'image du personnage est l'image "amochée" (gandalf-wounded.png et balrog-wounded.png).

Bonus n°2 : classes de personnages
----------------------------------
L'idée est de proposer au joueur parmis 3 classes de personnages :
- **magicien gris**
- **magicien blanc**
- **dieu**

Chaque classe a une particularité :
- le **magicien gris** est rapide, son **initiative** est majorée de **1D6%**. Par exemple s'il lance 1D6 et qu'il obtient 4, il aura 4% de bonus.
- l'attaque du balrog contre le **magicien blanc** est minorée de **1D10%**, car il est plus endurant.
- l'attaque du **dieu** est majorée de **1D10%**, il possède un sort de boule de feu très puissant.

Bonus n°3 : jauges de points de vie
-----------------------------------
Afficher les points de vie restants de chaque personnage dans une jauge qui reflète le pourcentage de points de vie restants.
S'il reste à un personnage la moitié de ses points de vie de départ, la jauge sera remplie à 50%.