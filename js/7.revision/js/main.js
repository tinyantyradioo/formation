'use strict';

/*** 1. 
 * Déclarez deux variables.
 * Affecter 2 nombres à ces variables
 * Afficher la somme de ces deux variables dans la console
*/


/*** 2.
 * Affecter le résultat de l'addition précédente dans une nouvelle variable
 * Afficher dans la console le type de donnée présent dans cette variable
*/


/*** 3. 
 * Demandez le nom de l'utilisateur.
 * Afficher dans la console "Hello nom !!!"
 * en utilisant la variable texte
*/

let texte = 'Hello';


/*** 4. Tableau
 * Afficher le texte 'Tu es la meilleur' dans la console
 * en utilisant le tableau "textes"
*/

let textes = ['tu','es','le','meilleur'];


/*** 5. Un petit peu plus
 * Afficher l'addition  de chaque élement avec 5 dans la console
 */

let numbers = [5,46,124,42,98];

/*** 6. Un petit peu plus
 * Ajouter 5 à chaque éléments dans le tableau
 */

let numbers = [5, 46, 124, 42, 98];

/*** 7. Multiplication
 * Affichez dans la console le résultat de la multiplication
 * de chaque nombre du tableau 1 avec les nombres du tableau 2
 */

let numbers1 = [5, 46, 124, 42, 98];
let numbers2 = [4, 8, 15, 16, 23, 42];

